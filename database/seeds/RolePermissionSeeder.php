<?php

use App\Models\Permission;
use App\Models\Role;
use Illuminate\Database\Seeder;

class RolePermissionSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $superadmin = new Role;
        $superadmin->name = 'superadmin';
        $superadmin->display_name = 'Super Administrator';
        $superadmin->description = 'Super Administrator';
        $superadmin->save();
        
        $admin = new Role;
        $admin->name = 'admin';
        $admin->display_name = 'Administrator';
        $admin->description = 'Administrator';
        $admin->save();
        
        $safety = new Role;
        $safety->name = 'safety';
        $safety->display_name = 'Surveior Safety';
        $safety->description = 'Surveior Safety';
        $safety->save();
        
        $pegawai = new Role;
        $pegawai->name = 'employee';
        $pegawai->display_name = 'Pegawai';
        $pegawai->description = 'Pegawai';
        $pegawai->save();
    }
}
