<?php

use Illuminate\Database\Seeder;
use App\Models\User;
use App\Models\Department;
use App\Models\Role;

class UserSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $departemen = new Department;
        $departemen->name = 'Human Resource Development (HRD)';
        $departemen->save();

        $role = Role::where('name','admin')->first();

        $user = new User;
        $user->name = 'Admin';
        $user->department_id = $departemen->id;
        $user->bn = '0001';
        $user->email = 'admin@example.com';
        $user->password = bcrypt('12345');
        $user->save();

        $user->attachRole($role);
    }
}
