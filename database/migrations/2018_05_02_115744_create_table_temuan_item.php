<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateTableTemuanItem extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('temuan_items', function (Blueprint $table) {
            $table->increments('id');
            $table->unsignedInteger('temuan_id');
            $table->unsignedInteger('hazard_class');
            $table->text('description');
            $table->text('corrective_action');
            $table->string('department_responsible')->nullable();
            $table->string('wo_needed')->nullable();
            $table->date('completion_date_est')->nullable();
            $table->date('completion_date_act')->nullable();
            $table->string('person_completing')->nullable();
            $table->string('standard')->nullable();
            $table->string('photo')->nullable();
            $table->string('photo_after')->nullable();
            $table->timestamps();

            $table->foreign('temuan_id')->references('id')->on('temuans')
                ->onUpdate('cascade')->onDelete('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('temuan_items');
    }
}
