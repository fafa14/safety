<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateChecklistItemsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('checklist_items', function (Blueprint $table) {
            $table->increments('id');
            $table->unsignedInteger('checklist_id');
            $table->unsignedInteger('category_id');
            $table->unsignedInteger('master_checklist_id');
            $table->text('category');
            $table->text('question');
            $table->boolean('answer')->nullable();
            $table->string('photo')->nullable();
            $table->integer('order');
            $table->boolean('skip')->default(false);
            $table->boolean('answered')->default(false);
            $table->text('detail')->nullable();
            $table->timestamps();

            $table->foreign('checklist_id')->references('id')->on('checklists')
                ->onUpdate('cascade')->onDelete('cascade');
            $table->foreign('category_id')
                ->references('id')
                ->on('categories')
                ->onDelete('cascade');
            $table->foreign('master_checklist_id')
                ->references('id')
                ->on('master_checklists')
                ->onDelete('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('checklist_items', function (Blueprint $table) {
            $table->dropForeign('checklist_items_checklist_id_foreign');
            $table->dropForeign('checklist_items_category_id_foreign');
            $table->dropForeign('checklist_items_master_checklist_id_foreign');
        });

        Schema::dropIfExists('checklist_items');
    }
}
