<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('admin', function () {
    return view('auth_template');
});

Auth::routes();

Route::get('/', 'HomeController@index')->name('home');
Route::get('/home', 'HomeController@index')->name('home');

Route::resource('categories', 'CategoryController')->except(['show']);
Route::resource('masterchecklists', 'MasterChecklistController')->except(['show']);
Route::resource('users', 'UserController')->except(['show']);
Route::resource('nearmisses', 'NearmissController');
Route::resource('locations', 'LocationController')->except(['show']);
Route::resource('departments', 'DepartmentController')->except(['show']);

Route::resource('checklists', 'ChecklistController');
Route::resource('checklistitems', 'ChecklistItemController')->only(['edit','update']);
Route::resource('temuans', 'TemuanController');
Route::resource('temuanitems', 'TemuanItemController');

Route::group(['prefix' => '/', 'middleware' => ['role:admin|superadmin']], function() {

    Route::get('/users/{id}/change-roles', 'UserController@changeRole')->name('change-roles');
    Route::post('/users/{id}/save-roles', 'UserController@saveRole')->name('save-roles');
    Route::get('/users/{id}/loginbyid', 'UserController@loginById')->name('loginbyid');
    Route::get('/users/{id}/reset-password', 'UserController@resetPassword')->name('reset-password');

    Route::get('mastercheklists-by-category/{id}', 'MasterChecklistController@indexByCategory')->name('mastercheklists-by-category');
    Route::get('mastercheklists-create-by-category/{id}', 'MasterChecklistController@createByCategory')->name('mastercheklists-create-by-category');

    Route::put('/departments/{id}/sethead/{user_id}', 'DepartmentController@sethead')->name('sethead');
});

Route::group(['prefix' => '/', 'middleware' => ['auth']], function() {
    Route::get('/change-password/user', 'ChangePasswordController@create')->name('change-password.create');
    Route::post('/change-password/user', 'ChangePasswordController@store')->name('change-password.store');

    Route::get('/checklists/{id}/print', 'ChecklistController@print')->name('checklists.print');
    Route::get('/temuans/{id}/print', 'TemuanController@print')->name('temuans.print');
    Route::get('/nearmisses/{id}/print', 'NearmissController@print')->name('nearmisses.print');
    Route::get('/temuans/{id}/create-by-checklist', 'TemuanController@createByChecklist')->name('temuans.createbychecklist');
    Route::get('/temuanitems/{id}/create-by-temuan', 'TemuanItemController@createByTemuan')->name('temuanitems.createbytemuan');
});

Route::group(['prefix' => '/', 'middleware' => ['role:safety']], function (){
    Route::post('checklistitems/skip-action/{id}/{action}', 'ChecklistItemController@skip')->name('checklistitems.skip');
});
Route::group(['prefix' => '/', 'middleware' => ['role:admin|superadmin|safety']], function() {
    Route::get('walkdowninspectors/walkdown/{id}/list', 'WalkdownInspectorController@list')->name('walkdowninspectors.list');
    Route::post('walkdowninspectors/walkdown/{id}/add', 'WalkdownInspectorController@add')->name('walkdowninspectors.add');
    Route::delete('walkdowninspectors/{id}/delete', 'WalkdownInspectorController@destroy')->name('walkdowninspectors.destroy');
});