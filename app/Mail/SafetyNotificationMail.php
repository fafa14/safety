<?php

namespace App\Mail;

use App\Models\Nearmiss;
use Illuminate\Bus\Queueable;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Support\Facades\Auth;

class SafetyNotificationMail extends Mailable
{
    use Queueable, SerializesModels;

    public $nearmiss;
    public $user;

    /**
     * Create a new message instance.
     *
     * @return void
     */
    public function __construct(Nearmiss $nearmiss)
    {
        $this->nearmiss = $nearmiss;
        $this->user = Auth::user();
    }

    /**
     * Build the message.
     *
     * @return $this
     */
    public function build()
    {
        return $this
                ->view('mail.safety_notification')
                ->from('noreply@safety.com')
                ->subject('Notifikasi data nearmiss baru');
    }
}
