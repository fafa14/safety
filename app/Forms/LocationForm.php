<?php

namespace App\Forms;

use App\Models\User;
use Kris\LaravelFormBuilder\Form;

class LocationForm extends Form
{
    public function buildForm()
    {
        $this
            ->add('name', 'text', [
                'label' => 'Name',
                'rules' => 'required'
            ])
            ->add('user_id', 'entity', [
                'label' => 'Person Responsible',
                'class' => 'App\Models\User',
                'property' => 'name',
                'query_builder' => function (User $user) {
                    return $user;
                }
            ])
            ->add('submit', 'submit');
    }
}
