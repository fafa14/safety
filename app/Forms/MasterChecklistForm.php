<?php

namespace App\Forms;

use Kris\LaravelFormBuilder\Form;

class MasterChecklistForm extends Form
{
    public function buildForm()
    {
        $this->add('question', 'textarea', [
                'label' => 'Pertanyaan',
                'rules' => 'required'
            ])
            ->add('category_id', 'hidden', [
                'label' => 'Kategori Id'
            ])
            ->add('submit', 'submit');
    }
}
