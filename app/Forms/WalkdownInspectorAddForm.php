<?php

namespace App\Forms;

use App\Models\User;
use Kris\LaravelFormBuilder\Form;

class WalkdownInspectorAddForm extends Form
{
    public function buildForm()
    {
        $this
            ->add('user_id', 'choice', [
                'label'     => 'Inspector',
                'choices'   => $this->getChoices(),
                'choice_option' => [
                    'wrapper'       => ['class' => 'choice-wrapper'],
                    'label_attr'    => ['class' => 'label-class']
                ]
            ])
            ->add('submit', 'submit');
    }

    private function getChoices()
    {
        $checklist = $this->getData('checklist');

        $ids = array();
        $selected_inspectors = $checklist->inspectors;
        foreach ($selected_inspectors as $selected_inspector) {
            array_push($ids, $selected_inspector->user_id);
        }

        $arr = array();
        $users = User::all();
        foreach ($users as $user) {
            if(!in_array($user->id, $ids)){
                $arr[$user->id] = $user->name.' ('.$user->department->name.')';
            }
        }
        return $arr;
    }
}
