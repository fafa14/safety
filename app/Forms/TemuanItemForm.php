<?php

namespace App\Forms;

use App\Models\TemuanItem;
use Kris\LaravelFormBuilder\Form;

class TemuanItemForm extends Form
{
    public function buildForm()
    {
        $this
            ->add('hazard_class', 'choice', [
                'choices' => TemuanItem::$hazardClassArray,
                'label' => 'Hazard Class',
                'rules' => 'required'
            ])
            ->add('description', 'textarea', [
                'label' => 'Description of Condition found',
                'rules' => 'required'
            ])
            ->add('photo', 'file', [
                'label' => 'Photo',
                'attr' => ['accept' => 'image/*' ],
            ])
            ->add('corrective_action', 'textarea', [
                'label' => 'Corrective Action',
                'rules' => 'required'
            ])
            ->add('photo_after', 'file', [
                'label' => 'Photo Corrective',
                'attr' => ['accept' => 'image/*' ],
            ])
            ->add('department_responsible', 'text', [
                'label' => 'Department Responsible',
            ])
            ->add('completion_date_est', 'date', [
                'label' => 'Estimated Completion Date',
                'attr' => ['class' => 'form-control datepicker'],
            ])
            ->add('wo_needed', 'text', [
                'label' => 'WO Needed',
            ])
            ->add('completion_date_act', 'date', [
                'label' => 'Actual Completion Date',
                'attr' => ['class' => 'form-control datepicker'],
            ])
            ->add('person_completing', 'text', [
                'label' => 'Person Completing',
            ])
            ->add('standard', 'text', [
                'label' => 'Standard',
            ])
            ->add('temuan_id', 'hidden')
            ->add('submit', 'submit');
    }
}
