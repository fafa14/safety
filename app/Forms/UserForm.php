<?php

namespace App\Forms;

use App\Models\Department;
use Kris\LaravelFormBuilder\Form;

class UserForm extends Form
{
    public function buildForm()
    {   
        $this->add('name', 'text', [
                'label' => 'Nama',
                'rules' => 'required|min:5'
            ])
            ->add('bn', 'text', [
                'label' => 'BN',
                'rules' => 'required'
            ])
            ->add('email', 'text', [
                'label' => 'Email',
                'rules' => 'required|email'
            ])
            ->add('department_id', 'entity', [
                'label' => 'Departemen',
                'class' => 'App\Models\Department',
                'property' => 'name',
                'query_builder' => function (Department $department) {
                    return $department;
                }
            ])
            ->add('submit', 'submit');
    }
}
