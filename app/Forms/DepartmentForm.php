<?php

namespace App\Forms;

use Kris\LaravelFormBuilder\Form;

class DepartmentForm extends Form
{
    public function buildForm()
    {
        $this
            ->add('name', 'text', [
                'label' => 'Nama',
                'rules' => 'required'
            ])
            ->add('submit', 'submit');
    }
}
