<?php

namespace App\Forms;

use Kris\LaravelFormBuilder\Form;

class ChecklistItemForm extends Form
{
    public function buildForm()
    {
        $this
            ->add('answer', 'choice', [
                'choices' => [true => 'Ya', false => 'Tidak'],
                'label' => 'Jawaban',
                'rules' => 'required'
            ])
            ->add('photo', 'file', [
                'label' => 'Foto',
                'attr' => ['accept' => 'image/*' ],
            ])
            ->add('detail', 'textarea', [
                'label' => 'Detail'
            ])
            ->add('submit', 'submit');
    }
}
