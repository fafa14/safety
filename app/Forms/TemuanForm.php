<?php

namespace App\Forms;

use App\Models\Department;
use App\Models\Location;
use Kris\LaravelFormBuilder\Form;

class TemuanForm extends Form
{
    public function buildForm()
    {
        $this
        ->add('location_id', 'entity', [
            'label' => 'Lokasi Kejadian',
            'class' => 'App\Models\Location',
            'property' => 'name',
            'query_builder' => function (Location $location) {
                return $location;
            }
        ])
        ->add('location_detail', 'text', [
            'label' => 'Area',
            'rules' => 'required'
        ])
        ->add('inspection_date', 'date', [
            'label' => 'Tanggal',
            'attr' => ['class' => 'form-control datepicker'],
            'rules' => 'required'
        ])
        ->add('checklist_id', 'hidden')
        ->add('submit', 'submit');
    }
}
