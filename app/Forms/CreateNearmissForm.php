<?php

namespace App\Forms;

use App\Models\Location;
use Kris\LaravelFormBuilder\Form;

class CreateNearmissForm extends Form
{
    public function buildForm()
    {
        $this
            ->add('location_id', 'entity', [
                'label' => 'Lokasi Kejadian',
                'class' => 'App\Models\Location',
                'property' => 'name',
                'query_builder' => function (Location $location) {
                    return $location;
                }
            ])
            ->add('location_detail', 'text', [
                'label' => 'Area',
                'rules' => 'required'
            ])
            ->add('penanggung_jawab', 'text', [
                'label' => 'Penanggung Jawab',
            ])
            ->add('ndate', 'text', [
                'label' => 'Tanggal Kejadian',
                'attr' => ['class' => 'form-control datepicker'],
                'rules' => 'required'
            ])
            ->add('ntime', 'text', [
                'label' => 'Waktu Kejadian',
                'attr' => ['class' => 'form-control timepicker'],
                'rules' => 'required'
            ])
            ->add('description', 'textarea', [
                'label' => 'Deskripsi Kejadian',
                'rules' => 'required'
            ])
            ->add('immediate_action', 'textarea', [
                'label' => 'Penananan Langsung',
                'rules' => 'required'
            ])
            ->add('photo', 'file', [
                'label' => 'Foto',
                'attr' => ['accept' => 'image/*' ],
            ])
            ->add('user_id', 'hidden')
            ->add('submit', 'submit');
    }
}
