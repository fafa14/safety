<?php

namespace App\Forms;

use Kris\LaravelFormBuilder\Form;

class ChangePasswordForm extends Form
{
    public function buildForm()
    {
        $this->add('password', 'repeated', [
            'type' => 'password',
            'second_name' => 'password_confirmation',
            'first_options' => [
                'label' => 'Password Baru',
                'rules' => 'required'
            ],
            'second_options' => [
                'label' => 'Ulangi Password Baru',
                'rules' => 'required'
            ],
        ])
        ->add('submit', 'submit');
    }
}
