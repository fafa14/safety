<?php

namespace App\Http\Controllers;

use App\Models\Checklist;
use App\Models\Nearmiss;
use Illuminate\Http\Request;

class HomeController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $page_title = 'Dashboard';

        $count_all_nearmiss = Nearmiss::all()->count();
        $count_all_walkdown = Checklist::all()->count();

        return view('home', compact('page_title', 'count_all_nearmiss', 'count_all_walkdown'));
    }
}
