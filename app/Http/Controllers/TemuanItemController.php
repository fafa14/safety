<?php

namespace App\Http\Controllers;

use App\Forms\TemuanItemForm;
use App\Models\Temuan;
use App\Models\TemuanItem;
use Illuminate\Http\Request;
use Kris\LaravelFormBuilder\FormBuilderTrait;

class TemuanItemController extends Controller
{
    use FormBuilderTrait;

    public function __construct()
    {
        $this->middleware('auth');
        $this->middleware('role:admin|superadmin|safety');
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }


    /**
     * @param $id
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function createByTemuan($id)
    {
        $temuan = Temuan::find($id);
        $item = new TemuanItem;
        $item->temuan_id = $temuan->id;

        $form = $this->form(TemuanItemForm::class, [
            'method'    => 'POST',
            'url' => route('temuanitems.store'),
            'model' => $item
        ]);

        return view('temuanitems.create', compact('form', 'page_title'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $form = $this->form(TemuanItemForm::class);

        if (!$form->isValid()) {
            return redirect()->back()->withErrors($form->getErrors())->withInput();
        }

        $data = $form->getFieldValues();
        $item = new TemuanItem;
        $item->temuan_id = $data['temuan_id'];
        $item->hazard_class = $data['hazard_class'];
        $item->description = $data['description'];
        $item->corrective_action = $data['corrective_action'];
        $item->department_responsible = $data['department_responsible'];
        $item->completion_date_est = $data['completion_date_est'];
        $item->wo_needed = $data['wo_needed'];
        $item->completion_date_act = $data['completion_date_act'];
        $item->person_completing = $data['person_completing'];
        $item->standard = $data['standard'];
        $item->setFile($form->getRequest()->file('photo'));
        $item->setFileAfter($form->getRequest()->file('photo_after'));
        $item->save();

        flash('Data item temuan berhasil disimpan.')->success();
        return redirect()->route('temuans.show', ['id'=>$item->temuan_id]);
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $page_title = 'Edit Item Temuan #'.$id;
        $item = TemuanItem::find($id);

        $form = $this->form(TemuanItemForm::class, [
            'method'    => 'PUT',
            'url' => route('temuanitems.update', ['id' => $id]),
            'model' => $item
        ]);

        return view('temuanitems.edit', compact('form', 'page_title'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $form = $this->form(TemuanItemForm::class);

        if (!$form->isValid()) {
            return redirect()->back()->withErrors($form->getErrors())->withInput();
        }

        $data = $form->getFieldValues();
        $item = TemuanItem::find($id);
        $item->hazard_class = $data['hazard_class'];
        $item->description = $data['description'];
        $item->corrective_action = $data['corrective_action'];
        $item->department_responsible = $data['department_responsible'];
        $item->completion_date_est = $data['completion_date_est'];
        $item->wo_needed = $data['wo_needed'];
        $item->completion_date_act = $data['completion_date_act'];
        $item->person_completing = $data['person_completing'];
        $item->standard = $data['standard'];
        $item->setFile($form->getRequest()->file('photo'));
        $item->setFileAfter($form->getRequest()->file('photo_after'));
        $item->save();

        flash('Data berhasil diubah.')->success();
        return redirect()->route('temuans.show', ['id'=>$item->temuan_id]);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $item = TemuanItem::find($id);
        $temuan_id = $item->temuan_id;
        $item->delete();

        flash('Data berhasil dihapus.')->success();
        return redirect()->route('temuans.show', ['id'=>$temuan_id]);
    }
}
