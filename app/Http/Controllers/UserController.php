<?php

namespace App\Http\Controllers;

use App\Forms\ChangePasswordForm;
use App\Forms\UserForm;
use App\Models\Role;
use App\Models\User;
use Illuminate\Http\Request;
use Illuminate\Http\Response;
use Illuminate\Support\Facades\Auth;
use Kris\LaravelFormBuilder\FormBuilderTrait;
use function view;

class UserController extends Controller
{
    use FormBuilderTrait;

    public function __construct()
    {
        $this->middleware('auth');
        $this->middleware('role:admin|superadmin');
    }

    /**
     * Display a listing of the resource.
     *
     * @return Response
     */
    public function index()
    {   
        $page_title = 'Daftar Pengguna';
        $page_description = 'Management data pengguna';
        
        $users = User::all();
        
        return view('users.index', compact('users', 'page_title', 'page_description'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return Response
     */
    public function create()
    {
        $page_title = 'Buat Pengguna Baru';
        
        $form = $this->form(UserForm::class, [
            'method' => 'POST',
            'enctype'   => 'multipart/form-data',
            'url' => route('users.store')
        ]);
        
        return view('users.create', compact('form', 'page_title'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  Request  $request
     * @return Response
     */
    public function store(Request $request)
    {
        $form = $this->form(UserForm::class);
        
        if (!$form->isValid()) {
            return redirect()->back()->withErrors($form->getErrors())->withInput();
        }
        
        $data = $form->getFieldValues();
        $default_role = Role::where('name', '=', 'employee')->first();
        
        $user = new User();
        $user->name = $data['name'];
        $user->email = $data['email'];
        $user->bn = $data['bn'];
        $user->department_id = $data['department_id'];
        $user->password = bcrypt('rahasia');
        $user->save();

        $user->attachRole($default_role);

        flash('Data user berhasil disimpan.')->success();
        return redirect('/users');

    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return Response
     */
    public function edit($id)
    {
        $user = User::find($id);
        $form = $this->form(UserForm::class, [
            'method'    => 'PUT',
            'enctype'   => 'multipart/form-data',
            'url'       => route('users.update', ['id' => $user->id]),
            'model'     => $user
        ]);

        $page_title = 'Edit User #'.$user->id;

        return view('users.edit', compact('form', 'page_title'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  Request  $request
     * @param  int  $id
     * @return Response
     */
    public function update(Request $request, $id)
    {
        $form = $this->form(UserForm::class);
        
        if (!$form->isValid()) {
            return redirect()->back()->withErrors($form->getErrors())->withInput();
        }
        
        $data = $form->getFieldValues();
        $user = User::find($id);
        $user->name = $data['name'];
        $user->bn = $data['bn'];
        $user->department_id = $data['department_id'];
        $user->email = $data['email'];
        $user->save();

        flash('Data berhasil diubah.')->success();
        return redirect('/users');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return Response
     */
    public function destroy($id)
    {
        $user = User::find($id);
        $user->delete();

        flash('Data berhasil dihapus.')->success();
        return redirect('/users');
    }
    
    public function changeRole($id){
        
        $user = User::find($id);
        $user_roles = [];
        
        foreach ($user->roles as $role) {
            $user_roles[] = $role->id;
        }

        $roles = Role::all();
        
        $page_title = 'Edit Role User';
        return view('users.changerole', compact('user', 'user_roles','roles', 'page_title'));
    }
    
    public function saveRole(Request $request, $id){
        $user = User::find($id);
        $user_roles = $user->roles;
        foreach ($user_roles as $value) {
            $user->detachRole($value);
        }
        
        $roles = $request->input('role');
        foreach ($roles as $role_id) {
            $role = Role::find($role_id);
            $user->attachRole($role);
        }

        flash(sprintf('Role untuk %s berhasil diubah.', $user->name))->success();
        return redirect('/users');
    }

    public function loginById(Request $request, $id){

        $user = User::findOrFail($id);
        Auth::loginUsingId($id);

        flash(sprintf('Login diubah dari Admin menjadi %s.', $user->name))->important();
        return redirect('/');
    }

    public function resetPassword($id)
    {
        $default_password = 'rahasia';

        $user = User::findOrFail($id);
        $user->password = bcrypt($default_password);
        $user->save();

        flash(sprintf('Password untuk akun %s berhasil di reset menjadi (%s).', $user->name, $default_password));
        return redirect()->back();
    }
}
