<?php

namespace App\Http\Controllers;

use App\Forms\CategoryForm;
use App\Models\Category;
use Illuminate\Http\Request;
use Illuminate\Http\Response;
use Kris\LaravelFormBuilder\FormBuilderTrait;

class CategoryController extends Controller
{
    use FormBuilderTrait;

    public function __construct()
    {
        $this->middleware('auth');
        $this->middleware(['role:admin|superadmin']);
    }

    /**
     * Display a listing of the resource.
     *
     * @return Response
     */
    public function index()
    {
        $page_title = 'Daftar Kategori';
        $page_description = 'Kategori checklist';
        
        $categories = Category::all();
        
        return view('categories.index', compact('categories', 'page_title', 'page_description'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return Response
     */
    public function create()
    {
        $page_title = 'Buat Kategori';
        
        $form = $this->form(CategoryForm::class, [
            'method' => 'POST',
            'url' => route('categories.store')
        ]);
        
        return view('categories.create', compact('form', 'page_title'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  Request  $request
     * @return Response
     */
    public function store(Request $request)
    {
        $form = $this->form(CategoryForm::class);
        
        if (!$form->isValid()) {
            return redirect()->back()->withErrors($form->getErrors())->withInput();
        }
        
        $data = $form->getFieldValues();
        $category = new Category();
        $category->name = $data['name'];
        $category->save();

        flash('Data berhasil disimpan.')->success();
        return redirect('/categories');
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return Response
     */
    public function edit($id)
    {
        $category = Category::find($id);
        $page_title = 'Edit Kategori #'.$category->id;
        
        $form = $this->form(CategoryForm::class, [
            'method'    => 'PUT',
            'url' => route('categories.update', ['id' => $category->id]),
            'model' => $category
        ]);
        
        return view('categories.edit', compact('form', 'page_title'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  Request  $request
     * @param  int  $id
     * @return Response
     */
    public function update(Request $request, $id)
    {
        $form = $this->form(CategoryForm::class);
        
        if (!$form->isValid()) {
            return redirect()->back()->withErrors($form->getErrors())->withInput();
        }
        
        $data = $form->getFieldValues();
        $category = Category::find($id);
        $category->name = $data['name'];
        $category->save();

        flash('Data berhasil diubah.')->success();
        return redirect('/categories');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return Response
     */
    public function destroy($id)
    {
        $category = Category::find($id);
        $category->delete();

        flash('Data berhasil dihapus.')->success();
        return redirect('/categories');
    }
}
