<?php

namespace App\Http\Controllers;

use App\Forms\WalkdownInspectorAddForm;
use App\Models\Checklist;
use App\Models\WalkdownInspector;
use Illuminate\Http\Request;
use Kris\LaravelFormBuilder\FormBuilderTrait;

class WalkdownInspectorController extends Controller
{
    use FormBuilderTrait;

    public function __construct()
    {
        $this->middleware('auth');
        $this->middleware('role:admin|superadmin|safety');
    }

    public function list($id)
    {
        $page_title = 'Daftar Walkdown Inspector';
        $checklist = Checklist::find($id);
        $form = $this->form(WalkdownInspectorAddForm::class, [
            'method'    => 'POST',
            'url'       => route('walkdowninspectors.add', ['id' => $id])
        ],[
            'checklist' => $checklist
        ]);

        return view('walkdowninspectors.list', compact('page_title', 'checklist', 'form'));
    }

    public function add($id)
    {
        $checklist = Checklist::find($id);
        $form = $this->form(WalkdownInspectorAddForm::class,[],[
            'checklist' => $checklist
        ]);

        if (!$form->isValid()) {
            return redirect()->back()->withErrors($form->getErrors())->withInput();
        }

        $data = $form->getFieldValues();
        $wi = new WalkdownInspector();
        $wi->checklist_id = $id;
        $wi->user_id = $data['user_id'];
        $wi->save();

        flash('Data berhasil disimpan.')->success();
        return redirect()->back();
    }

    public function destroy($id)
    {
        $wi = WalkdownInspector::find($id);
        $wi->delete();

        flash('Data berhasil dihapus.')->success();
        return redirect()->back();
    }
}
