<?php

namespace App\Http\Controllers;

use App\Forms\ChecklistItemForm;
use App\Models\ChecklistItem;
use Illuminate\Http\Request;
use Kris\LaravelFormBuilder\FormBuilderTrait;

class ChecklistItemController extends Controller
{
    use FormBuilderTrait;

    public function __construct()
    {
        $this->middleware('auth');
        $this->middleware('role:admin|superadmin|safety');
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $item = ChecklistItem::find($id);
        $page_title = 'Item checklist #'.$item->id;

        $form = $this->form(ChecklistItemForm::class, [
            'method'    => 'PUT',
            'url' => route('checklistitems.update', ['id' => $item->id]),
            'model' => $item
        ]);

        return view('checklistitems.edit', compact('form', 'page_title', 'item'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $form = $this->form(ChecklistItemForm::class);

        if (!$form->isValid()) {
            return redirect()->back()->withErrors($form->getErrors())->withInput();
        }

        $data = $form->getFieldValues();
        $item = ChecklistItem::find($id);
        $item->detail = $data['detail'];
        $item->answer = $data['answer'];
        $item->answered = true;
        $item->setFile($form->getRequest()->file('photo'));
        $item->save();

        $nextItem = $item->getNextUnanswered();
        if($nextItem == null){
            return redirect()->route('checklists.show', ['id' => $item->checklist_id]);
        }

        flash('Data berhasil diubah.')->success();
        return redirect()->route('checklistitems.edit', ['id' => $nextItem->id]);
    }

    public function skip(Request $request, $id, $action = 'skip'){

        $category = null;
        $items = ChecklistItem::where('category_id', $id)->get();
        foreach ($items as $item)
        {
            if($category == null) $category = $item->category;

            if($action == 'skip') {
                $item->skip = 1;
                $item->answer = null;
                $item->unlink();
                $item->photo = null;
                $item->detail = null;
                $item->answered = 0;
            }else{
                $item->skip = 0;
            }
            $item->save();
        }

        $tanda = $action == 'skip' ? 'tidak diisi' : 'harus diisi';
        flash(sprintf('Kategori %s berhasil ditandai untuk %s.', $category, $tanda))->success();

        return redirect()->back();
    }
}
