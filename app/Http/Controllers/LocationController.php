<?php

namespace App\Http\Controllers;

use App\Forms\LocationForm;
use App\Models\Location;
use Illuminate\Http\Request;
use Kris\LaravelFormBuilder\FormBuilderTrait;
use function view;

class LocationController extends Controller
{
    use FormBuilderTrait;

    public function __construct()
    {
        $this->middleware('auth');
        $this->middleware('role:admin|superadmin');
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $page_title = 'Daftar Lokasi';        
        $locations = Location::all();
        
        return view('locations.index', compact('locations', 'page_title'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $page_title = 'Tambah Lokasi';
        
        $form = $this->form(LocationForm::class, [
            'method' => 'POST',
            'url' => route('locations.store')
        ]);
        
        return view('locations.create', compact('form', 'page_title'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $form = $this->form(LocationForm::class);
        
        if (!$form->isValid()) {
            return redirect()->back()->withErrors($form->getErrors())->withInput();
        }
        
        $data = $form->getFieldValues();
        $location = new Location();
        $location->name = $data['name'];
        $location->user_id = $data['user_id'];
        $location->save();

        flash('Data berhasil disimpan.')->success();
        return redirect('/locations');
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $location = Location::find($id);
        $page_title = 'Edit Lokasi #'.$location->id;
        
        $form = $this->form(LocationForm::class, [
            'method'    => 'PUT',
            'url' => route('locations.update', ['id' => $location->id]),
            'model' => $location
        ]);
        
        return view('locations.edit', compact('form', 'page_title'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $form = $this->form(LocationForm::class);
        
        if (!$form->isValid()) {
            return redirect()->back()->withErrors($form->getErrors())->withInput();
        }
        
        $data = $form->getFieldValues();
        $location = Location::find($id);
        $location->name = $data['name'];
        $location->user_id = $data['user_id'];
        $location->save();

        flash('Data berhasil diubah.')->success();
        return redirect('/locations');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $location = Location::find($id);
        $location->delete();

        flash('Data berhasil dihapus.')->success();
        return redirect('/locations');
    }
}
