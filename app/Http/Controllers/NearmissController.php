<?php

namespace App\Http\Controllers;

use App\Forms\CreateNearmissForm;
use App\Mail\SafetyNotificationMail;
use App\Models\Nearmiss;
use App\Models\User;
use Illuminate\Http\Request;
use Illuminate\Http\Response;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Mail;
use Kris\LaravelFormBuilder\FormBuilderTrait;

class NearmissController extends Controller
{
    use FormBuilderTrait;

    public function __construct()
    {
        $this->middleware('auth');
    }

    /**
     * Display a listing of the resource.
     *
     * @return Response
     */
    public function index()
    {
        $page_title = 'Daftar Laporan Hazard Nearmiss';

        if(Auth::user()->hasRole('employee')){
            $nearmisses = Auth::user()->nearmissCreated;
        }else{
            $nearmisses = Nearmiss::all();
        }
        
        return view('nearmisses.index', compact('nearmisses', 'page_title'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return Response
     */
    public function create()
    {
        $page_title = 'Buat Laporan Hazard Nearmiss';
        
        $user = Auth::user();
        $nearmiss = new Nearmiss();
        $nearmiss->user_id = $user->id;
        
        $form = $this->form(CreateNearmissForm::class, [
            'method' => 'POST',
            'url' => route('nearmisses.store'),
            'model' => $nearmiss
        ]);
        
        return view('nearmisses.create', compact('form', 'page_title'));
    }

    /**
     * @param $id
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function show($id){

        $nearmiss = Nearmiss::findOrFail($id);
        $page_title = 'Detail data hazard nearmiss #'.$id;

        return view('nearmisses.show', compact('nearmiss', 'page_title'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  Request  $request
     * @return Response
     */
    public function store(Request $request)
    {
        $form = $this->form(CreateNearmissForm::class);
        
        if (!$form->isValid()) {
            return redirect()->back()->withErrors($form->getErrors())->withInput();
        }

        // mengambil semua user dengan role safety
        $safety_users = User::whereHas('roles', function ($query) {
            $query->where('name', '=', 'safety');
        })->get();
        
        $data = $form->getFieldValues();
        $nearmiss = new Nearmiss();
        $nearmiss->location_id = $data['location_id'];
        $nearmiss->location_detail = $data['location_detail'];
        $nearmiss->user_id = $data['user_id'];
        $nearmiss->penanggung_jawab = $data['penanggung_jawab'];
        $nearmiss->ndate = $data['ndate'];
        $nearmiss->ntime = $data['ntime'];
        $nearmiss->description = $data['description'];
        $nearmiss->immediate_action = $data['immediate_action'];
        $nearmiss->setFile($form->getRequest()->file('photo'));
        $nearmiss->save();

        // Kirim email notifikasi
        Mail::to($safety_users)->send(new SafetyNotificationMail($nearmiss));

        flash('Data nearmiss berhasil disimpan.')->success();
        return redirect('/nearmisses');
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return Response
     */
    public function edit($id)
    {
        $nearmiss = Nearmiss::find($id);
        $form = $this->form(CreateNearmissForm::class, [
            'method'    => 'PUT',
            'enctype'   => 'multipart/form-data',
            'url'       => route('nearmisses.update', ['id' => $nearmiss->id]),
            'model'     => $nearmiss
        ]);

        $page_title = 'Edit Hazard Nearmiss #'.$nearmiss->id;

        return view('nearmisses.edit', compact('form', 'page_title'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  Request  $request
     * @param  int  $id
     * @return Response
     */
    public function update(Request $request, $id)
    {
        $form = $this->form(CreateNearmissForm::class);
        
        if (!$form->isValid()) {
            return redirect()->back()->withErrors($form->getErrors())->withInput();
        }
        
        $data = $form->getFieldValues();
        $nearmiss = Nearmiss::find($id);
        $nearmiss->location_id = $data['location_id'];
        $nearmiss->location_detail = $data['location_detail'];
        $nearmiss->user_id = $data['user_id'];
        $nearmiss->penanggung_jawab = $data['penanggung_jawab'];
        $nearmiss->ndate = $data['ndate'];
        $nearmiss->ntime = $data['ntime'];
        $nearmiss->description = $data['description'];
        $nearmiss->immediate_action = $data['immediate_action'];
        $nearmiss->setFile($form->getRequest()->file('photo'));
        $nearmiss->save();

        flash('Data hazard nearmiss berhasil diubah.')->success();
        return redirect('/nearmisses');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return Response
     */
    public function destroy($id)
    {
        $nearmiss = Nearmiss::find($id);
        $nearmiss->delete();

        flash('Data berhasil dihapus.')->success();;
        return redirect('/nearmisses');
    }
    
    public function print($id)
    {
        $nearmiss = Nearmiss::find($id);
        $type = 'Nearmiss';
        
        return view('nearmisses.print', compact('nearmiss', 'type'));
    }
}
