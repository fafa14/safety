<?php

namespace App\Http\Controllers;

use App\Forms\DepartmentForm;
use App\Models\Department;
use App\Models\User;
use Illuminate\Http\Request;
use Kris\LaravelFormBuilder\FormBuilderTrait;

class DepartmentController extends Controller
{
    use FormBuilderTrait;

    public function __construct()
    {
        $this->middleware('auth');
        $this->middleware('role:admin|superadmin');
    }

    /**
     * Menampilkan daftar departemen
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $page_title = 'Daftar Departemen';        
        $departments = Department::all();
        
        return view('departments.index', compact('departments', 'page_title'));
    }

    /**
     * Menampilkan form buat departemen baru
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $page_title = 'Tambah Departemen';
        
        $form = $this->form(DepartmentForm::class, [
            'method' => 'POST',
            'url' => route('departments.store')
        ]);
        
        return view('departments.create', compact('form', 'page_title'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $form = $this->form(DepartmentForm::class);
        
        if (!$form->isValid()) {
            return redirect()->back()->withErrors($form->getErrors())->withInput();
        }
        
        $data = $form->getFieldValues();
        $department = new Department();
        $department->name = $data['name'];
        $department->save();

        flash('Data berhasil disimpan.')->success();
        return redirect('/departments');
    }


    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $department = Department::find($id);
        $page_title = 'Edit Departemen #'.$department->id;
        $users = $department->users;
        $head = User::find($department->user_id);

        $form = $this->form(DepartmentForm::class, [
            'method'    => 'PUT',
            'url' => route('departments.update', ['id' => $department->id]),
            'model' => $department
        ]);
        
        return view('departments.edit', compact('form', 'page_title', 'users', 'head', 'department'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $form = $this->form(DepartmentForm::class);
        
        if (!$form->isValid()) {
            return redirect()->back()->withErrors($form->getErrors())->withInput();
        }
        
        $data = $form->getFieldValues();
        $department = Department::find($id);
        $department->name = $data['name'];
        $department->save();

        flash('Data berhasil diubah.')->success();
        return redirect('/departments');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $department = Department::find($id);
        $department->delete();

        flash('Data berhasil hapus.')->success();
        return redirect('/departments');
    }
    
    public function sethead($id, $user_id)
    {
        $user = User::find($user_id);

        $department = Department::find($id);
        $department->user_id = $user_id;
        $department->save();

        flash(sprintf('%s berhasil di set sebagai kepala department %s', $user->name, $department->name))->success();
        return redirect()->route('departments.edit', ['id' => $id]);
    }
}
