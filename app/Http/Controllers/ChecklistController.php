<?php

namespace App\Http\Controllers;

use App\Forms\ChecklistForm;
use App\Models\Category;
use App\Models\Checklist;
use App\Models\ChecklistItem;
use App\Models\Temuan;
use App\Models\WalkdownInspector;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Kris\LaravelFormBuilder\FormBuilderTrait;

class ChecklistController extends Controller
{
    use FormBuilderTrait;

    public function __construct()
    {
        $this->middleware('auth');
        $this->middleware('role:admin|superadmin|safety');
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $page_title = 'Daftar Walkdown';
        $page_description = 'Walkdown';

        if(Auth::user()->hasRole('admin')){
            $checklists = Checklist::all();
        }else{
            $checklists = Auth::user()->checklistCreated;
        }

        return view('checklists.index', compact('checklists', 'page_title', 'page_description'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $page_title = 'Buat Walkdown';

        $form = $this->form(ChecklistForm::class, [
            'method' => 'POST',
            'url' => route('checklists.store')
        ]);

        return view('checklists.create', compact('form', 'page_title'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $form = $this->form(ChecklistForm::class);

        if (!$form->isValid()) {
            return redirect()->back()->withErrors($form->getErrors())->withInput();
        }

        $data = $form->getFieldValues();

        // create checklist
        $checklist = new Checklist;
        $checklist->user_id = Auth::user()->id;
        $checklist->location_id = $data['location_id'];
        $checklist->location_detail = $data['location_detail'];
        $checklist->inspection_date = $data['inspection_date'];
        $checklist->save();

        $wi = new WalkdownInspector();
        $wi->checklist_id = $checklist->id;
        $wi->user_id = Auth::user()->id;
        $wi->save();

        // create temuan
        $temuan = new Temuan;
        $temuan->user_id = Auth::user()->id;
        $temuan->checklist_id = $checklist->id;
        $temuan->location_id = $data['location_id'];
        $temuan->location_detail = $data['location_detail'];
        $temuan->inspection_date = $data['inspection_date'];
        $temuan->save();

        //get category
        $checklist_categories = Category::all();
        $order = 1;
        //save checklist item
        foreach ($checklist_categories as $category) {

            // get master checklist
            $mcs = $category->mastercheklists;
            foreach ($mcs as $mc) {
                $item = new ChecklistItem;
                $item->checklist_id = $checklist->id;
                $item->category_id = $category->id;
                $item->master_checklist_id = $mc->id;
                $item->category = $category->name;
                $item->question = $mc->question;
                $item->order = $order;
                $item->save();

                $order++;
            }
        }

        flash('Data berhasil disimpan.')->success();
        return redirect('/checklists');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $checklist = Checklist::find($id);
        $page_title = 'Daftar Item Cheklist';

        return view('checklists.show', compact('checklist', 'page_title'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $checklist = Checklist::find($id);
        $page_title = 'Edit Walkdown #'.$checklist->id;

        $form = $this->form(ChecklistForm::class, [
            'method'    => 'PUT',
            'url' => route('checklists.update', ['id' => $checklist->id]),
            'model' => $checklist
        ]);

        return view('checklists.edit', compact('form', 'page_title'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $form = $this->form(ChecklistForm::class);

        if (!$form->isValid()) {
            return redirect()->back()->withErrors($form->getErrors())->withInput();
        }

        $data = $form->getFieldValues();
        $checklist = Checklist::find($id);
        $checklist->location_id = $data['location_id'];
        $checklist->location_detail = $data['location_detail'];
        $checklist->inspection_date = $data['inspection_date'];
        $checklist->save();


        // update temuan
        $temuan = Temuan::where('checklist_id', $checklist->id)->first();
        $temuan->location_id = $data['location_id'];
        $temuan->location_detail = $data['location_detail'];
        $temuan->inspection_date = $data['inspection_date'];
        $temuan->save();

        flash('Data berhasil diubah.')->success();
        return redirect('/checklists');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $checklist = Checklist::find($id);
        $checklist->delete();

        flash('Data berhasil dihapus.')->success();
        return redirect('/checklists');
    }

    /**
     * @param $id
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function print($id)
    {
        $checklist = Checklist::find($id);
        $unanswered = $checklist->getItemUnanswered();
        if ($unanswered)
        {
            flash('Terdapat item checklist yang belum diisi, lengkapi pengisian checklist sebelum mencetak.')->error();
            return redirect()->back();
        }

        return view('checklists.print', compact('checklist'));
    }
}
