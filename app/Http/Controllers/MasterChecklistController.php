<?php

namespace App\Http\Controllers;

use App\Forms\MasterChecklistForm;
use App\Models\MasterChecklist;
use App\Models\Category;
use Illuminate\Http\Request;
use Kris\LaravelFormBuilder\FormBuilderTrait;

class MasterChecklistController extends Controller
{
    use FormBuilderTrait;

    public function __construct()
    {
        $this->middleware('auth');
        $this->middleware('role:admin|superadmin');
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function indexByCategory($id)
    {
        $category = Category::find($id);
        
        $page_title = 'Daftar Master Ceklist';
        $masterchecklists = $category->mastercheklists;
        
        return view('masterchecklists.index', compact('masterchecklists', 'category', 'page_title'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function createByCategory($id)
    {
        $category = Category::where('id', $id)->first();
        $page_title = 'Buat Master Checklist';
        $page_description = 'Untuk kategori '.$category->name;
        $masterchecklist = new MasterChecklist();
        $masterchecklist->category_id = $category->id;
        
        $form = $this->form(MasterChecklistForm::class, [
            'method' => 'POST',
            'model' => $masterchecklist,
            'url' => route('masterchecklists.store')
        ]);
        
        return view('masterchecklists.create', compact('form', 'page_title', 'page_description'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $form = $this->form(MasterChecklistForm::class);
        
        if (!$form->isValid()) {
            return redirect()->back()->withErrors($form->getErrors())->withInput();
        }
        
        $data = $form->getFieldValues();
        
        $masterchecklist = new MasterChecklist();
        $masterchecklist->question = $data['question'];
        $masterchecklist->category_id = $data['category_id'];
        $masterchecklist->save();

        flash('Data berhasil disimpan.')->success();
        return redirect('/mastercheklists-by-category/'.$data['category_id']);
    }


    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $masterchecklist = MasterChecklist::find($id);
        $page_title = 'Edit Ceklist #'.$masterchecklist->id;
        
        $form = $this->form(MasterChecklistForm::class, [
            'method'    => 'PUT',
            'url' => route('masterchecklists.update', ['id' => $masterchecklist->id]),
            'model' => $masterchecklist
        ]);
        
        return view('masterchecklists.edit', compact('form', 'page_title'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $form = $this->form(MasterChecklistForm::class);
        
        if (!$form->isValid()) {
            return redirect()->back()->withErrors($form->getErrors())->withInput();
        }
        
        $data = $form->getFieldValues();
        $masterchecklist = MasterChecklist::find($id);
        $masterchecklist->question = $data['question'];
        $masterchecklist->save();

        flash('Data berhasil diubah.')->success();
        return redirect('/mastercheklists-by-category/'.$data['category_id']);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {   
        $masterchecklist = MasterChecklist::find($id);
        $category_id = $masterchecklist->category_id;
        $masterchecklist->delete();

        flash('Data berhasil dihapus.')->success();
        return redirect('/mastercheklists-by-category/'.$category_id);
    }
}
