<?php

namespace App\Http\Controllers;

use App\Forms\TemuanForm;
use App\Models\Checklist;
use App\Models\Temuan;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Kris\LaravelFormBuilder\FormBuilderTrait;

class TemuanController extends Controller
{
    use FormBuilderTrait;
    public function __construct()
    {
        $this->middleware('auth');
        $this->middleware('role:admin|superadmin|safety');
    }
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $page_title = 'Daftar Temuan';
        $page_description = 'Temuan';

        if(Auth::user()->hasRole('admin')){
            $temuans = Temuan::all();
        }else{
            $temuans = Auth::user()->temuanCreated;
        }

        return view('temuans.index', compact('temuans', 'page_title', 'page_description'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function createByChecklist($id)
    {
        $page_title = 'Buat temuan';
        $checklist = Checklist::find($id);
        $temuan = new Temuan;
        $temuan->checklist_id = $checklist->id;
        $temuan->location_id = $checklist->location_id;
        $temuan->inspection_date = $checklist->inspection_date;

        $form = $this->form(TemuanForm::class, [
            'method'    => 'POST',
            'url' => route('temuans.store'),
            'model' => $temuan
        ]);

        return view('temuans.create', compact('form', 'page_title'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $form = $this->form(TemuanForm::class);

        if (!$form->isValid()) {
            return redirect()->back()->withErrors($form->getErrors())->withInput();
        }

        $data = $form->getFieldValues();
        $temuan = new Temuan;
        $temuan->user_id = Auth::user()->id;
        $temuan->checklist_id = $data['checklist_id'];
        $temuan->department_id = $data['department_id'];
        $temuan->location_id = $data['location_id'];
        $temuan->inspection_date = $data['inspection_date'];
        $temuan->save();

        flash('Data temuan berhasil disimpan.')->success();
        return redirect('/temuans');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $temuan = Temuan::find($id);
        $page_title = 'Daftar Item Temuan';

        return view('temuans.show', compact('temuan', 'page_title'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }

    public function print($id)
    {
        $temuan = Temuan::find($id);
        $type = 'Temuan';

        return view('temuans.print', compact('temuan', 'type'));
    }
}
