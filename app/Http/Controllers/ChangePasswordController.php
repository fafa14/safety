<?php
/**
 * Created by PhpStorm.
 * User: muhfadjar
 * Date: 20/07/18
 * Time: 13:12
 */

namespace App\Http\Controllers;


use App\Forms\ChangePasswordForm;
use App\Models\User;
use Kris\LaravelFormBuilder\FormBuilderTrait;

class ChangePasswordController extends Controller
{
    use FormBuilderTrait;

    public function __construct()
    {
        $this->middleware('auth');
    }

    public function create()
    {
        $user = auth()->user();
        $page_title = 'Ganti Password';

        $form = $this->form(ChangePasswordForm::class, [
            'method' => 'POST',
            'url' => route('change-password.store')
        ]);

        return view('changepassword.create', compact('page_title', 'form'));
    }

    public function store()
    {
        $user = auth()->user();
        $form = $this->form(ChangePasswordForm::class);

        if (!$form->isValid()) {
            return redirect()->back()->withErrors($form->getErrors())->withInput();
        }

        $data = $form->getFieldValues();

        if($data['password'] != $data['password_confirmation']){
            flash('Konfirmasi password baru anda tidak sesuai dengan password yang anda input')->error()->important();
            return redirect()->back();
        }

        if(bcrypt($data['password']) == $user->password)
        {
            flash('Password baru sama dengan password sebelummya')->error();
            return redirect()->back();
        }

        $user->password = bcrypt($data['password']);
        $user->save();

        flash('Password berhasil diubah.')->success();
        return redirect('/');
    }
}