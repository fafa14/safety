<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Temuan extends Model
{
    public function creator()
    {
        return $this->belongsTo('App\Models\User', 'user_id');
    }

    public function location()
    {
        return $this->belongsTo('App\Models\Location');
    }

    public function temuanItems()
    {
        return $this->hasMany('App\Models\TemuanItem', 'temuan_id', 'id');
    }

    public function checklist()
    {
        return $this->belongsTo('App\Models\Checklist');
    }
}
