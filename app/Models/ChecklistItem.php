<?php

namespace App\Models;

use App\Models\Traits\Uploadable;
use Illuminate\Database\Eloquent\Model;
use Intervention\Image\Facades\Image;

class ChecklistItem extends Model
{
    use Uploadable;

    private $upload_dir = 'storage/images/checklistitems';
    private $file;

    public function getFile(){
        return $this->file;
    }

    public function setFile($file) {
        $this->file = $file;
    }

    public function tumbnail()
    {
        return $this->getTumbnail($this->photo, $this->upload_dir);
    }

    public function link()
    {
        return $this->getImage($this->photo, $this->upload_dir);
    }


    public function save(array $options = array()) {
        $this->upload();
        parent::save($options);
    }

    public function upload()
    {
        $this->photo = $this->uploadImage($this->getFile(), $this->upload_dir);
    }

    public function unlink() {
        $this->unlinkFile($this->photo, $this->upload_dir);
    }

    public function delete() {
        $this->unlink();
        parent::delete();
    }

    public function checklist()
    {
        return $this->belongsTo('App\Models\Checklist', 'checklist_id');
    }

    public function category_data()
    {
        return $this->belongsTo('App\Models\Category', 'category_id');
    }

    public function master_checklist()
    {
        return $this->belongsTo('App\Models\MasterChecklist', 'master_checklist_id');
    }

    public function answerString(){

        if($this->skip){
            return 'tidak diisi';
        } elseif($this->answered == false){
            return 'belum diisi';
        }

        return $this->answer ? 'Ya' : 'Tidak';

    }

    public function getNextUnanswered(){

        $item = ChecklistItem::where([
                    'checklist_id' => $this->checklist_id,
                    'category_id' => $this->category_id,
                    'answered' => false,
                    'skip' => false
                ])
            ->orderBy('order', 'asc')
            ->first();

        if(!$item)
        {
            $item = ChecklistItem::where([
                'checklist_id' => $this->checklist_id,
                'answered' => false,
                'skip' => false
            ])
                ->orderBy('order', 'asc')
                ->first();
        }

        return $item;

    }
}
