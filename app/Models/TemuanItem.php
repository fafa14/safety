<?php

namespace App\Models;

use App\Models\Traits\Uploadable;
use Illuminate\Database\Eloquent\Model;
use Intervention\Image\Facades\Image;

class TemuanItem extends Model
{
    use Uploadable;

    public static $hazardClassArray = array(
        1 => 'Low',
        2 => 'Medium',
        3 => 'High'
    );

    public function hazardClassString(){
        return self::$hazardClassArray[$this->hazard_class];
    }

    private $upload_dir = 'storage/images/temuanitems';
    private $file;
    private $file_after;

    public function getFile(){
        return $this->file;
    }

    public function setFile($file) {
        $this->file = $file;
    }

    public function getFileAfter(){
        return $this->file_after;
    }

    public function setFileAfter($file) {
        $this->file_after = $file;
    }

    public function tumbnail()
    {
        return $this->getTumbnail($this->photo, $this->upload_dir);
    }

    public function tumbnailAfter()
    {
        return $this->getTumbnail($this->photo_after, $this->upload_dir);
    }

    public function link()
    {
        return $this->getImage($this->photo, $this->upload_dir);
    }

    public function linkAfter()
    {
        return $this->getImage($this->photo_after, $this->upload_dir);
    }

    public function save(array $options = array()) {
        $this->upload();
        parent::save($options);
    }

    public function upload()
    {
        $this->photo = $this->uploadImage($this->getFile(), $this->upload_dir);
        $this->photo_after = $this->uploadImage($this->getFileAfter(), $this->upload_dir);
    }

    public function unlink() {
        $this->unlinkFile($this->photo, $this->upload_dir);
        $this->unlinkFile($this->photo_after, $this->upload_dir);
    }

    public function delete() {
        $this->unlink();
        parent::delete();
    }

    public function temuan()
    {
        return $this->belongsTo('App\Models\Temuan', 'temuan_id');
    }

}
