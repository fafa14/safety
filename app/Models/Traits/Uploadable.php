<?php
/**
 * Created by PhpStorm.
 * User: root
 * Date: 29/06/18
 * Time: 7:05
 */

namespace App\Models\Traits;

use Intervention\Image\Facades\Image;

trait Uploadable
{
    /**
     * @param $file
     * @param $directory
     * @return string|void
     */
    private function uploadImage($file, $directory){
        if(null === $file){
            return;
        }

        $name = \hash('sha1', time().$file->getClientOriginalName()).'.'.$file->getClientOriginalExtension();
        $file->move($directory, $name);
        $this->createTumbnail($name, $directory, 200, 200);

        $file = null;
        return $name;
    }

    private function uploadFile($file, $directory){
        if(null === $file){
            return;
        }

        $name = \hash('sha1', time().$file->getClientOriginalName()).'.'.$file->getClientOriginalExtension();
        $file->move($directory, $name);

        $file = null;
        return $name;
    }

    private function unlinkFile($name, $directory) {
        if(null !== $name)
        {
            unlink(public_path($directory.'/'.$name));
        }
    }

    private function createTumbnail($name, $directory, $width, $height){
        $img = Image::make($directory.'/'.$name);
        $img->resize($height, $width);
        $img->save($directory.'/tumbnails/'.$name);
    }

    private function unlinkImage($name, $directory) {
        if(null !== $name)
        {
            unlink(public_path($directory.'/'.$name));
            unlink(public_path($directory.'/tumbnails/'.$name));
        }
    }

    private function getTumbnail($name, $directory){
        return $name ? $directory.'/tumbnails/'.$name : $directory.'/tumbnails/default.jpg';
    }

    private function getImage($name, $directory){
        return $name ? $directory.'/'.$name : $directory.'/default.jpg';
    }
}
