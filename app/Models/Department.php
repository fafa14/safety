<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Department extends Model
{
    
    /**
     * Ambil data User pada departement
     * 
     * @return Array
     */
    public function users() 
    {
        return $this->hasMany('App\Models\User');
    }

}
