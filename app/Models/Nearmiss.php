<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Intervention\Image\Facades\Image;

class Nearmiss extends Model
{
    private $upload_dir = 'storage/images/nearmisses';
    private $file;
    
    public function getFile(){
        return $this->file;
    }
    
    public function setFile($file) {
        $this->file = $file;
    }
    
    public function tumbnail() {
        return $this->photo ? $this->upload_dir.'/tumbnails/'.$this->photo : $this->upload_dir.'/tumbnails/default.jpg';
    }
    
    public function link() {
        return $this->photo ? $this->upload_dir.'/'.$this->photo : $this->upload_dir.'/default.jpg';
    }
    
    public function save(array $options = array()) {
        $this->upload();
        parent::save($options);
    }
    
    public function upload() {
        if (null === $this->getFile()) {
            return;
        }
        $name = \hash('sha1', time().$this->getFile()->getClientOriginalName()).'.'.$this->getFile()->getClientOriginalExtension();
        $this->getFile()->move(
            $this->upload_dir,
            $name
        );

        $this->createTumbnail($name, 300, 300);

        // set the path property to the filename where you've saved the file
        $this->photo = $name;
        // clean up the file property as you won't need it anymore
        $this->file = null;
    }
    
    private function createTumbnail($name, $width, $height){
        Image::make($this->upload_dir.'/'.$name,array(
            'width' => $width,
            'height' => $height
        ))->save($this->upload_dir.'/tumbnails/'.$name);
    }

    public function unlink() {
        if($this->photo != '')
        {
            unlink(public_path($this->upload_dir.'/'.$this->photo));
            unlink(public_path($this->upload_dir.'/tumbnails/'.$this->photo));
        }
    }
    
    public function delete() {
        $this->unlink();
        parent::delete();
    }

    public function creator()
    {
        return $this->belongsTo('App\Models\User', 'user_id');
    }
    
    public function location()
    {
        return $this->belongsTo('App\Models\Location');
    }

}
