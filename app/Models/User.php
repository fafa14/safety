<?php

namespace App\Models;

use Illuminate\Foundation\Auth\User as Authenticatable;
use Illuminate\Notifications\Notifiable;
use Intervention\Image\Facades\Image;
use Zizaco\Entrust\Traits\EntrustUserTrait;

class User extends Authenticatable
{
    use Notifiable, EntrustUserTrait;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'name', 'department_id', 'bn', 'email', 'password'
    ];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'password', 'remember_token',
    ];
    
    public function nearmissCreated() {
        return $this->hasMany('App\Models\Nearmiss', 'user_id', 'id');
    }

    public function checklistCreated() {
        return $this->hasMany('App\Models\Checklist', 'user_id', 'id');
    }

    public function temuanCreated() {
        return $this->hasMany('App\Models\Temuan', 'user_id', 'id');
    }
    
    public function department()
    {
        return $this->belongsTo('App\Models\Department');
    }

    public function locationResponsible()
    {
        return $this->hasMany('App\Models\Location');
    }

    public function getUserTheme(){
        if ($this->hasRole('admin') || $this->hasRole('superadmin')){
            return 'skin-red';
        }elseif ($this->hasRole('safety')){
            return 'skin-green';
        }else{
            return 'skin-blue';
        }
    }
}
