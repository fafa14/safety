<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Location extends Model
{
    public function nearmiss()
    {
        return $this->hasMany('App\Models\Nearmiss');
    }

    public function checklists()
    {
        return $this->hasMany('App\Models\Checklist');
    }

    public function responsiblePerson()
    {
        return $this->belongsTo('App\Models\User','user_id');
    }
}
