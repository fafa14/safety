<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Checklist extends Model
{


    public function creator()
    {
        return $this->belongsTo('App\Models\User', 'user_id');
    }

    public function checklistItems()
    {
        return $this->hasMany('App\Models\ChecklistItem', 'checklist_id', 'id');
    }

    public function inspectors()
    {
        return $this->hasMany('App\Models\WalkdownInspector', 'checklist_id', 'id');
    }

    public function indexedInspector()
    {
        $data = array();
        foreach ($this->inspectors as $inspector) {
            $user = $inspector->inspector;
            $data[$user->department->name][$user->id] = $user->name;
        }
        return $data;
    }

    public function getItemsIndexByCategory(){

        $arr = array();
        foreach ($this->checklistItems as $checklistItem) {
            $arr[$checklistItem->category][] = $checklistItem;
        }

        return $arr;
    }

    public function getChecklistItemCategory(){

        $arr = array();
        foreach ($this->checklistItems as $checklistItem)
        {
            if(!$checklistItem->skip)
            {
                $arr[$checklistItem->category_id] = $checklistItem->category;
            }
        }

        return $arr;
    }

    public function getSkipedCategories(){

        $arr = array();
        foreach ($this->checklistItems as $checklistItem)
        {
            if($checklistItem->skip)
            {
                $arr[$checklistItem->category_id] = $checklistItem->category;
            }
        }

        return $arr;
    }

    public function getChecklistItemsByCategoryId($id){

        $arr = array();
        foreach ($this->checklistItems as $checklistItem)
        {
            if($checklistItem->category_id == $id)
            {
                $arr[$checklistItem->id] = $checklistItem;
            }
        }

        return $arr;
    }

    public function getItemsIndexByCategoryForPrint(){

        $arr = array();
        foreach ($this->checklistItems as $checklistItem)
        {
            if(!$checklistItem->skip) {
                $arr[$checklistItem->category][] = $checklistItem;
            }
        }

        return $arr;
    }

    public function getItemUnanswered(){

        $item = ChecklistItem::where(['checklist_id' => $this->id, 'answered' => false, 'skip' => false])
            ->orderBy('order', 'asc')
            ->first();
        return $item;

    }

    public function location()
    {
        return $this->belongsTo('App\Models\Location');
    }

    public function temuan()
    {
        return $this->hasOne('App\Models\Temuan');
    }
}
