<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class WalkdownInspector extends Model
{
    public function checklist()
    {
        return $this->belongsTo('App\Models\Checklist', 'checklist_id');
    }

    public function inspector()
    {
        return $this->belongsTo('App\Models\User', 'user_id');
    }
}
