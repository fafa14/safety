<aside class="main-sidebar">

    <!-- sidebar: style can be found in sidebar.less -->
    <section class="sidebar">

      <!-- Sidebar Menu -->
      <ul class="sidebar-menu" data-widget="tree">
        <li class="header">Master</li>
        <!-- Optionally, you can add icons to the links -->
        @role('admin', 'superadmin')
            <li><a href="{{ route('users.index') }}">
                    <i class="fa fa-users"></i> 
                    <span>User</span>
                </a>
            </li>
            <li class="treeview">
                <a href="#">
                  <i class="fa fa-folder-open"></i> <span>Master Data</span>
                  <span class="pull-right-container">
                    <i class="fa fa-angle-left pull-right"></i>
                  </span>
                </a>
                <ul class="treeview-menu" style="display: none;">
                    <li>
                        <a href="{{ route('categories.index') }}">
                            <i class="fa fa-list"></i> 
                            <span>Master Ceklist</span>
                        </a>
                    </li>
                    <li><a href="{{ route('departments.index') }}">
                            <i class="fa fa-location-arrow"></i> 
                            <span>Daftar Departemen</span>
                        </a>
                    </li>
                    <li><a href="{{ route('locations.index') }}">
                            <i class="fa fa-location-arrow"></i> 
                            <span>Daftar Lokasi</span>
                        </a>
                    </li>
                </ul>
            </li>
            <li>
                <a href="{{ route('nearmisses.index') }}">
                    <i class="fa fa-bell"></i> 
                    <span>Laporan Hazard Nearmiss</span>
                </a>
            </li>
              <li>
                  <a href="{{ route('checklists.index') }}">
                      <i class="fa fa-check-circle"></i>
                      <span>Laporan Walkdown</span>
                  </a>
              </li>
        @endrole
          @role('safety')
          <li>
              <a href="{{ route('nearmisses.index') }}">
                  <i class="fa fa-users"></i>
                  <span>Laporan Hazard Nearmiss</span>
              </a>
          </li>
          <li>
              <a href="{{ route('checklists.index') }}">
                  <i class="fa fa-check-circle"></i>
                  <span>Laporan Walkdown</span>
              </a>
          </li>
          @endrole
        @role('employee')
            <li>
                <a href="{{ route('nearmisses.index') }}">
                    <i class="fa fa-users"></i> 
                    <span>Laporan Hazard Nearmiss</span>
                </a>
            </li>
        @endrole
      </ul>
      <!-- /.sidebar-menu -->
    </section>
    <!-- /.sidebar -->
  </aside>