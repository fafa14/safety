<!-- Main Header -->
<header class="main-header">

<!-- Logo -->
<a href="{{ route('home') }}" class="logo">
  <!-- mini logo for sidebar mini 50x50 pixels -->
  <span class="logo-mini"><b>SF</b></span>
  <!-- logo for regular state and mobile devices -->
  <span class="logo-lg">{{ config('app.name') }}</span>
</a>

<!-- Header Navbar -->
<nav class="navbar navbar-static-top" role="navigation">
  <!-- Sidebar toggle button-->
  <a href="#" class="sidebar-toggle" data-toggle="push-menu" role="button">
    <span class="sr-only">Toggle navigation</span>
  </a>
  <!-- Navbar Right Menu -->
  <div class="navbar-custom-menu">
    <ul class="nav navbar-nav">
      <!-- Tasks Menu -->
      {{--<li class="dropdown tasks-menu">--}}
        {{--<!-- Menu Toggle Button -->--}}
        {{--<a href="#" class="dropdown-toggle" data-toggle="dropdown">--}}
          {{--<i class="fa fa-flag-o"></i>--}}
          {{--<span class="label label-danger">9</span>--}}
        {{--</a>--}}
        {{--<ul class="dropdown-menu">--}}
          {{--<li class="header">You have 9 tasks</li>--}}
          {{--<li>--}}
            {{--<!-- Inner menu: contains the tasks -->--}}
            {{--<ul class="menu">--}}
              {{--<li><!-- Task item -->--}}
                {{--<a href="#">--}}
                  {{--<!-- Task title and progress text -->--}}
                  {{--<h3>--}}
                    {{--Design some buttons--}}
                    {{--<small class="pull-right">20%</small>--}}
                  {{--</h3>--}}
                  {{--<!-- The progress bar -->--}}
                  {{--<div class="progress xs">--}}
                    {{--<!-- Change the css width attribute to simulate progress -->--}}
                    {{--<div class="progress-bar progress-bar-aqua" style="width: 20%" role="progressbar"--}}
                         {{--aria-valuenow="20" aria-valuemin="0" aria-valuemax="100">--}}
                      {{--<span class="sr-only">20% Complete</span>--}}
                    {{--</div>--}}
                  {{--</div>--}}
                {{--</a>--}}
              {{--</li>--}}
              {{--<!-- end task item -->--}}
            {{--</ul>--}}
          {{--</li>--}}
          {{--<li class="footer">--}}
            {{--<a href="#">View all tasks</a>--}}
          {{--</li>--}}
        {{--</ul>--}}
      {{--</li>--}}
      <!-- User Account Menu -->
      <li class="dropdown user user-menu">
        <!-- Menu Toggle Button -->
        <a href="#" class="dropdown-toggle" data-toggle="dropdown">
          <i class="ion ion-android-person user-image"></i>
          <!-- hidden-xs hides the username on small devices so only the image appears. -->
          <span class="hidden-xs">{{ Auth::user()->name }}</span>
        </a>
        <ul class="dropdown-menu">
          <!-- The user image in the menu -->
          <li class="user-header">
            <p>
              {{ Auth::user()->name }}
              <small>
                  
                  @foreach( Auth::user()->roles as $role )
                    <a class="btn btn-default btn-xs" href="javascript:void(0)">{{ $role->name }}</a>
                  @endforeach
                  
              </small>
            </p>
          </li>
          <!-- Menu Footer-->
          <li class="user-footer">
            <div class="pull-left">
              <a href="{{ route('change-password.create') }}"
                 class="btn btn-default btn-flat">Ganti Password</a>
            </div>
            <div class="pull-right">
              <a class="btn btn-default btn-flat" href="{{ route('logout') }}"
                onclick="event.preventDefault();
                              document.getElementById('logout-form').submit();">
                 Logout
             </a>

             <form id="logout-form" action="{{ route('logout') }}" method="POST" style="display: none;">
                 @csrf
             </form>
            </div>
          </li>
        </ul>
      </li>
    </ul>
  </div>
</nav>
</header>