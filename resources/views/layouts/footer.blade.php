<footer class="main-footer">
    <!-- To the right -->
    <div class="pull-right hidden-xs">
        Safety Walkdown and Nearmiss Application
    </div>
    <!-- Default to the left -->
    <strong>Copyright © 2018 <a href="javascript:void(0)">{{ config('app.name') }}</a>.</strong> All rights reserved.
</footer>