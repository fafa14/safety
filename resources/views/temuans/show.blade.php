@extends('admin_template')

@section('content')

    <div class="box">
        <div class="box-header">
            <h3 class="box-title">
                Detail Temuan
                <a href="{{ route('temuans.print', ['id'=>$temuan->id]) }}" target="_blank"
                   class="btn btn-sm btn-default">
                    <i class="fa fa-print"></i> Cetak
                </a>
            </h3>
        </div>
        <div class="box-body">
            <table id="temuanTable" class="table table-bordered table-striped">
                <tr>
                    <th>ID</th>
                    <td>{{ $temuan->id }}</td>
                </tr>
                <tr>
                    <th>Inspectors</th>
                    <td>
                        @php($checklist = $temuan->checklist )
                        @include( 'walkdowninspectors.inspector_list')
                    </td>
                </tr>
                <tr>
                    <th>Date</th>
                    <td>{{ $temuan->inspection_date }}</td>
                </tr>
                <tr>
                    <th>Location</th>
                    <td>{{ $temuan->location->name }}</td>
                </tr>
                <tr>
                    <th>Area</th>
                    <td>{{ $temuan->location_detail }}</td>
                </tr>
            </table>
        </div>
    </div>

    <div class="box">
        <div class="box-header">
            <h3 class="box-title">
                Daftar Item Temuan
                @if( auth()->user()->hasRole('safety') )
                    <a href="{{ route('temuanitems.createbytemuan', ['id'=>$temuan->id]) }}"
                       title="Tambah Baru"
                       class="btn btn-primary btn-sm">
                        <i class="fa fa-plus"></i>
                    </a>
                @endif
            </h3>
        </div>
        <div class="box-body" style="overflow-x: scroll">
            <table id="temuanItemTable" class="table table-bordered table-responsive">
                <thead>
                <tr>
                    <th width="20">No</th>
                    <th width="100">Hazard Class</th>
                    <th>Description of Cond. found</th>
                    <th>Corective Action</th>
                    <th>Detail</th>
                    @if( auth()->user()->hasRole('safety') )
                        <th width="150">Action</th>
                    @endif
                </tr>
                </thead>
                <tbody>
                    @foreach($temuan->temuanItems as $key => $item)
                        <tr>
                            <td>{{ $key + 1 }}</td>
                            <td>{{ $item->hazardClassString() }}</td>
                            <td>
                                {{ $item->description }} <br/>
                                @if( $item->photo )
                                    <a href="{{ asset($item->link()) }}" data-featherlight="image">
                                        <img class="img-circle" style="width: 50px; height: 50px;" src="{{ asset($item->tumbnail()) }}" alt="img"/>
                                    </a>
                                @endif
                            </td>
                            <td>
                                {{ $item->corrective_action }} <br/>
                                @if( $item->photo_after )
                                    <a href="{{ asset($item->linkAfter()) }}" data-featherlight="image">
                                        <img class="img-circle" style="width: 50px; height: 50px;" src="{{ asset($item->tumbnailAfter()) }}" alt="img"/>
                                    </a>
                                @endif
                            </td>
                            <td>
                                <dl>
                                    @if($item->department_responsible != null)
                                        <dt>Dept. Responsible</dt><dd>{{ $item->department_responsible }}</dd>
                                    @endif
                                    @if($item->completion_date_est != null)
                                        <dt>Est. Completion Date</dt><dd>{{ $item->completion_date_est }}</dd>
                                    @endif
                                </dl>
                            </td>
                            @if( auth()->user()->hasRole('safety') )
                            <td>
                                <a href="{{ route('temuanitems.edit', ['id'=>$item->id]) }}"
                                   title="Edit Data"
                                   class="btn btn-sm btn-primary">
                                    <i class="fa fa-edit"></i>
                                </a>
                                <form onsubmit="return confirm('Hapus data?');"
                                      id="delete-{{ $item->id }}"
                                      class="hide"
                                      method="POST"
                                      action="{{ route('temuanitems.destroy', ['id'=>$item->id]) }}">
                                    {{ method_field('DELETE') }}
                                    {{ csrf_field() }}
                                </form>
                                <a href="javascript:void(0)"
                                   onclick="$('#delete-{{ $item->id }}').submit()"
                                   title="Hapus"
                                   class="btn btn-sm btn-danger">
                                    <i class="fa fa-remove"></i>
                                </a>
                            </td>
                                @endif
                        </tr>
                    @endforeach
                </tbody>
            </table>
        </div>
    </div>

@endsection