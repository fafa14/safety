@extends('admin_template')

@section('content')

    <div class="box">
        <div class="box-header">
            <h3 class="box-title">
                Tabel Temuan
            </h3>
        </div>
        <!-- /.box-header -->

        <div class="box-body" style="overflow-x: scroll">
            <table id="temuanTable" class="table table-bordered table-striped">
                <thead>
                <tr>
                    <th>ID</th>
                    <th>Location</th>
                    <th>Date</th>
                    <th>Action</th>
                </tr>
                </thead>
                <tbody>
                @foreach( $temuans as $temuan )
                    <tr>
                        <td>{{ $temuan->id }}</td>
                        <td>
                            {{ $temuan->location->name }}<br/>
                            <i class="small">{{ $temuan->location_detail }}</i>
                        </td>
                        <td>{{ $temuan->inspection_date }}</td>
                        <td>
                            <a href="{{ route('temuans.show', ['id'=>$temuan->id]) }}"
                               class="btn btn-sm btn-primary">
                                <i class="fa fa-book"></i>
                            </a>


                                {{--<a href="{{ route('checklists.print', ['id'=>$checklist->id]) }}"--}}
                                   {{--class="btn btn-sm btn-primary">--}}
                                    {{--<i class="fa fa-print"></i>--}}
                                {{--</a>--}}

                        </td>
                    </tr>
                @endforeach
                </tbody>
            </table>
        </div>
        <!-- /.box-body -->
    </div>

@endsection

@section('inline-script')
    <script type="text/javascript">
        $(document).ready(function(){
            $('#temuanTable').DataTable({
                responsive: true,
                'aoColumnDefs': [{
                    'bSortable': false,
                    'aTargets': [ -1 ]
                }]
            });
        });
    </script>
@endsection