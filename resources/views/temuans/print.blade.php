@extends('print_template')

@section('content')
    <div class="row">
        <div class="col-sm-12 invoice-col">
            <b>CETAK TEMUAN</b><br/><br/>
        </div>
    </div>

    <table class="table table-bordered table-striped">
        <tr>
            <th width="200">ID Temuan</th>
            <td>{{ $temuan->id }}</td>
        </tr>
        <tr>
            <th>Inspectors</th>
            <td>
                @php($checklist = $temuan->checklist )
                @include( 'walkdowninspectors.inspector_list')
            </td>
        </tr>
        <tr>
            <th>Date</th>
            <td>{{ $temuan->inspection_date }}</td>
        </tr>
        <tr>
            <th>Location</th>
            <td>{{ $temuan->location->name }}</td>
        </tr>
        <tr>
            <th>Area</th>
            <td>{{ $temuan->location_detail }}</td>
        </tr>
    </table>
    <br/>

    <table id="temuanItemTable" class="table table-bordered table-striped">
        <thead>
        <tr>
            <th width="20">No</th>
            <th>Hazard Class</th>
            <th>Description of Condition</th>
            <th>Corrective Action</th>
            <th>Person/Department Responsible</th>
            <th>Est. Completion Date</th>
            <th>Actual Completion Date</th>
            <th>WO Needed</th>
            <th>Person Completing</th>
            <th>Standard</th>
        </tr>
        </thead>
        <tbody>
        @php ($inc = 1)
        @foreach($temuan->temuanItems as $item)
            <tr>
                <td>{{ $inc }}</td>
                <td>{{ $item->hazardClassString() }}</td>
                <td>
                    {{ $item->description }} <br/>
                    <img style="width: 150px;" src="{{ asset($item->link()) }}" alt="img"/>
                </td>
                <td>
                    {{ $item->corrective_action }} <br/>
                    <img style="width: 150px;" src="{{ asset($item->linkAfter()) }}" alt="img"/>
                </td>
                <td>{{ $item->department_responsible }}</td>
                <td>{{ $item->completion_date_est }}</td>
                <td>{{ $item->completion_date_act }}</td>
                <td>{{ $item->wo_needed }}</td>
                <th>{{ $item->person_completing }}</th>
                <td>{{ $item->standard }}</td>
            </tr>
            @php($inc++)
        @endforeach
        </tbody>
    </table>
@endsection