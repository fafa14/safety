@extends('admin_template')

@section('content')

    <div class="box">
        <div class="box-header">
          <h3 class="box-title">
              Tabel Ceklist Kategori
              <a href="{{ route('categories.create') }}" 
                 class="btn btn-primary btn-sm">
                  <i class="fa fa-plus"></i>
              </a>
          </h3>
        </div>
        <!-- /.box-header -->
            
        <div class="box-body" style="overflow-x: scroll">
            <table id="categoryTable" class="table table-bordered table-striped">
                <thead>
                    <tr>
                        <th>ID</th>
                        <th>Nama</th>
                        <th>Action</th>
                    </tr>
                </thead>
                <tbody>
                    @foreach( $categories as $category )
                    <tr>
                        <td>{{ $category->id }}</td>
                        <td>{{ $category->name }}</td>
                        <td>
                            <a href="{{ route('categories.edit', ['id'=>$category->id]) }}" title="Edit"
                               class="btn btn-sm btn-primary">
                                <i class="fa fa-edit"></i>
                            </a>
                            <a href="{{ route('mastercheklists-by-category', ['id'=>$category->id]) }}" title="Daftar Master Checklist"
                               class="btn btn-sm btn-primary">
                                <i class="fa fa-list"></i>
                            </a>
                            <form onsubmit="return confirm('Menghapus kategory akan menghapus data ceklist yang ada, Hapus Kategori?');" 
                                  id="delete-{{ $category->id }}"
                                  class="hide"
                                  method="POST" 
                                  action="{{ route('categories.destroy', ['id'=>$category->id]) }}">
                                {{ method_field('DELETE') }}
                                {{ csrf_field() }}
                            </form>
                            <a href="javascript:void(0)" title="Hapus"
                               onclick="$('#delete-{{ $category->id }}').submit()"
                               class="btn btn-sm btn-danger">
                                <i class="fa fa-remove"></i>
                            </a>
                        </td>
                    </tr>
                    @endforeach
                </tbody>
            </table>
        </div>
        <!-- /.box-body -->
    </div>

@endsection

@section('inline-script')
    <script type="text/javascript">
        $(document).ready(function(){
            $('#categoryTable').DataTable({
                responsive: true,
                'aoColumnDefs': [{
                    'bSortable': false,
                    'aTargets': [ -1 ]
                }]
            });
        });
    </script>
@endsection