<!DOCTYPE html>
<!--
This is a starter template page. Use this page to start your new project from
scratch. This page gets rid of all links and provides the needed markup only.
-->
<html>
<head>
    <meta charset="UTF-8">
    <title>{{ $page_title or "Cetak" }}</title>
    <meta content='width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no' name='viewport'>
    <!-- Bootstrap 3.3.2 -->
    <link href="{{ asset("/bower_components/bootstrap/dist/css/bootstrap.min.css") }}" rel="stylesheet" type="text/css" />
    <!-- Font Awesome Icons -->
    <link href="{{ asset("/bower_components/font-awesome/css/font-awesome.min.css") }}" rel="stylesheet" type="text/css" />
    <!-- Font Awesome Icons -->
    <!-- Ionicons -->
    <link href="{{ asset("/bower_components/Ionicons/css/ionicons.min.css") }}" rel="stylesheet" type="text/css" />
    <link href="//code.jquery.com/ui/1.12.1/themes/base/jquery-ui.css" rel="stylesheet" type="text/css" />
    <!-- Theme style -->
    <link href="{{ asset("/dist/css/AdminLTE.min.css")}}" rel="stylesheet" type="text/css" />
    <link rel="stylesheet" href="https://fonts.googleapis.com/css?family=Source+Sans+Pro:300,400,600,700,300italic,400italic,600italic">
    <!--[if lt IE 9]>
    <script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
    <script src="https://oss.maxcdn.com/libs/respond.js/1.3.0/respond.min.js"></script>
    <![endif]-->
    <style>
        @media print {
            .menu-container {
                visibility: hidden;
            }
        }
        @media screen {
            .menu-container{
                margin: 10px 25px;
            }
        }
    </style>
</head>
<body>
<div class="wrapper" style="overflow: visible">

    <section class="menu-container">
        <button onclick="window.print();">Print</button>
        <button onclick="window.close();">Close</button>
    </section>

    <section class="invoice">
        
        <!-- header -->
        <div class="row">
            <div class="col-xs-12">
              <h2 class="page-header">
                <i class="fa fa-globe">Nama Perusahaan</i>
              </h2>
            </div>
        </div>
        
        <!-- info -->
        @yield('content')
    </section>

</div><!-- ./wrapper -->

<!-- AdminLTE App -->
<script src="{{ asset ("/dist/js/adminlte.min.js") }}" type="text/javascript"></script>

@yield('inline-script')
</body>
</html>