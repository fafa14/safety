@extends('admin_template')

@section('content')

    @role('safety')
        <div class="box">
            <div class="box-header">
            </div>
            <div class="box-body">
                {!! form($form) !!}
            </div>
            <!-- /.box-header -->
        </div>
    @endrole

    <div class="box">
        <div class="box-header">
        </div>
        <div class="box-body">
            <table id="userTable" class="table table-bordered table-striped">
                <thead>
                    <tr>
                        <th>No</th>
                        <th>Name</th>
                        <th>Departemen</th>
                        <th>Action</th>
                    </tr>
                </thead>
                <tbody>
                    @foreach( $checklist->inspectors as $key => $inspector )
                        @php( $user = $inspector->inspector )
                        <tr>
                            <td>{{ $key+1 }}</td>
                            <td>{{ $user->name }}</td>
                            <td>{{ $user->department->name }}</td>
                            <td>
                                @role('safety')
                                    @if( $user->id != $checklist->creator->id )
                                        <form class="hide" id="delete-{{ $inspector->id }}"
                                              onsubmit="return confirm('Hapus Inspector?');"
                                              method="POST" action="{{ route('walkdowninspectors.destroy', ['id'=>$inspector->id]) }}">
                                            {{ method_field('DELETE') }}
                                            {{ csrf_field() }}
                                        </form>
                                        <a href="javascript:void(0)"
                                           onclick="$('#delete-{{ $inspector->id }}').submit();"
                                           title="Hapus inspector dari daftar"
                                           class="btn btn-sm btn-danger">
                                            <i class="fa fa-remove"></i>
                                        </a>
                                    @endif
                                @endrole
                            </td>
                        </tr>
                    @endforeach
                </tbody>
            </table>
        </div>
        <!-- /.box-header -->
    </div>

@endsection
