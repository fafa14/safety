@foreach($checklist->indexedInspector() as $key => $users)
    <strong>{{ $key }}</strong><br/>
    @php($inc = 0)
    @foreach($users as $key2 => $user)
        {{ ++$inc }}. {{ $user }}
        @if( $checklist->creator->id == $key2 )
            <i class="fa fa-star"></i>
        @endif
        <br/>
    @endforeach
@endforeach