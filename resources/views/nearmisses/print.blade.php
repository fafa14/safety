@extends('print_template')

@section('content')
    <div class="row">
        <div class="col-sm-12 invoice-col">
            <b>CETAK HAZARD NEARMISS</b><br/><br/>
        </div>
    </div>

    <table id="nearmissTable" class="table table-bordered table-striped">
        <tr>
            <th>ID Nearmiss</th>
            <td>{{ $nearmiss->id }}</td>
        </tr>
        <tr>
            <th>Dibuat oleh</th>
            <td>{{ $nearmiss->creator->name }}</td>
        </tr>
        <tr>
            <th>Waktu kejadian</th>
            <td>{{ $nearmiss->ndate }} {{ $nearmiss->ntime }}</td>
        </tr>
        <tr>
            <th>Lokasi</th>
            <td>
                {{ $nearmiss->location->name }}
            </td>
        </tr>
        <tr>
            <th>Detail Lokasi</th>
            <td>
                {{ $nearmiss->location_detail }}
            </td>
        </tr>
        <tr>
            <th>Penanggung Jawab</th>
            <td>{{ $nearmiss->penanggung_jawab }}</td>
        </tr>
        <tr>
            <th>Deskripsi</th>
            <td>{{ $nearmiss->description }}</td>
        </tr>
        <tr>
            <th>Penanganan Langsung</th>
            <td>{{ $nearmiss->immediate_action }}</td>
        </tr>
        <tr>
            <th>Dokumentasi Foto</th>
            <td>
                <img width="300px" src="{{ asset($nearmiss->link()) }}" alt="{{ $nearmiss->photo }}" >
            </td>
        </tr>
    </table>
@endsection