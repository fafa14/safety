@extends('admin_template')

@section('content')

    <div class="box">
        <div class="box-header">
          <h3 class="box-title">
              Laporan Hazard Nearmiss
              <a href="{{ route('nearmisses.create') }}" title="Buat Baru"
                 class="btn btn-primary btn-sm">
                  <i class="fa fa-plus"></i>
              </a>
          </h3>
        </div>
        <!-- /.box-header -->
            
        <div class="box-body" style="overflow-x: scroll">
            <table id="nearmissTable" class="table table-bordered table-striped">
                <thead>
                    <tr>
                        <th>ID</th>
                        @role('admin', 'superadmin', 'safety')
                            <th>Pelapor</th>
                        @endrole
                        <th>Lokasi</th>
                        <th>Penanggung Jawab</th>
                        <th>Waktu Kejadian</th>
                        <th>Action</th>
                    </tr>
                </thead>
                <tbody>
                    @foreach( $nearmisses as $nearmiss )
                    <tr>
                        <td>{{ $nearmiss->id }}</td>
                        @role('admin', 'superadmin', 'safety')
                            <td>{{ $nearmiss->creator->name }}</td>
                        @endrole
                        <td>
                            {{ $nearmiss->location->name }}<br/>
                            <i class="small">{{ $nearmiss->location_detail }}</i>
                        </td>
                        <td>{{ $nearmiss->penanggung_jawab }}</td>
                        <td>{{ $nearmiss->ndate }} {{ $nearmiss->ntime }}</td>
                        <td>
                            <a href="{{ route('nearmisses.show', ['id'=>$nearmiss->id]) }}"
                               title="Detail"
                               class="btn btn-sm btn-primary">
                                <i class="fa fa-eye"></i>
                            </a>
                            <a href="{{ route('nearmisses.edit', ['id'=>$nearmiss->id]) }}"
                               title="Edit Data"
                               class="btn btn-sm btn-primary">
                                <i class="fa fa-edit"></i>
                            </a>
                            <a href="{{ route('nearmisses.print', ['id'=>$nearmiss->id]) }}" target="_blank"
                               title="Cetak"
                               class="btn btn-sm btn-default">
                                <i class="fa fa-print"></i>
                            </a>

                            @if( Auth::user()->hasRole('admin') or $nearmiss->user_id == Auth::user()->id )
                                <form onsubmit="return confirm('Hapus data?');"
                                      id="delete-{{ $nearmiss->id }}"
                                      class="hide"
                                      method="POST"
                                      action="{{ route('nearmisses.destroy', ['id'=>$nearmiss->id]) }}">
                                    {{ method_field('DELETE') }}
                                    {{ csrf_field() }}
                                </form>
                                <a href="javascript:void(0)"
                                   onclick="$('#delete-{{ $nearmiss->id }}').submit()"
                                   title="Hapus"
                                   class="btn btn-sm btn-danger">
                                    <i class="fa fa-remove"></i>
                                </a>
                            @endif
                        </td>
                    </tr>
                    @endforeach
                </tbody>
            </table>
        </div>
        <!-- /.box-body -->
    </div>

@endsection

@section('inline-script')
    <script type="text/javascript">
        $(document).ready(function(){
            $('#nearmissTable').DataTable( {
                responsive: true,
                'aoColumnDefs': [{
                    'bSortable': false,
                    'aTargets': [ -1 ]
                }]
            });
        });
    </script>
@endsection