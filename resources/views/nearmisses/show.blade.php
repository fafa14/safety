@extends('admin_template')

@section('content')

    <div class="box">
        <div class="box-header">
          <h3 class="box-title">
              Detail Hazard Nearmiss
          </h3>
        </div>
        <!-- /.box-header -->
            
        <div class="box-body">

            <div style="margin-bottom: 10px"; class="pull-right">
                <a href="{{ route('nearmisses.edit', ['id'=>$nearmiss->id]) }}"
                   class="btn btn-sm btn-primary">
                    <i class="fa fa-edit"></i>
                </a>
                <a href="{{ route('nearmisses.print', ['id'=>$nearmiss->id]) }}" target="_blank"
                   class="btn btn-sm btn-default">
                    <i class="fa fa-print"></i>
                </a>

                @if( Auth::user()->hasRole('admin') or $nearmiss->user_id == Auth::user()->id )
                    <form onsubmit="return confirm('Hapus data?');"
                          id="delete-{{ $nearmiss->id }}"
                          class="hide"
                          method="POST"
                          action="{{ route('nearmisses.destroy', ['id'=>$nearmiss->id]) }}">
                        {{ method_field('DELETE') }}
                        {{ csrf_field() }}
                    </form>
                    <a href="javascript:void(0)"
                       onclick="$('#delete-{{ $nearmiss->id }}').submit()"
                       class="btn btn-sm btn-danger">
                        <i class="fa fa-remove"></i>
                    </a>
                @endif
            </div>

            <table id="nearmissTable" class="table table-bordered table-responsive">
                <tr>
                    <th>ID</th>
                    <td>{{ $nearmiss->id }}</td>
                </tr>
                <tr>
                    <th>Dibuat oleh</th>
                    <td>{{ $nearmiss->creator->name }}</td>
                </tr>
                <tr>
                    <th>Waktu kejadian</th>
                    <td>{{ $nearmiss->ndate }} {{ $nearmiss->ntime }}</td>
                </tr>
                <tr>
                    <th>Lokasi</th>
                    <td>
                        {{ $nearmiss->location->name }}
                    </td>
                </tr>
                <tr>
                    <th>Detail Lokasi</th>
                    <td>
                        {{ $nearmiss->location_detail }}
                    </td>
                </tr>
                <tr>
                    <th>Penanggung Jawab</th>
                    <td>{{ $nearmiss->penanggung_jawab }}</td>
                </tr>
                <tr>
                    <th>Deskripsi</th>
                    <td>{{ $nearmiss->description }}</td>
                </tr>
                <tr>
                    <th>Penanganan Langsung</th>
                    <td>{{ $nearmiss->immediate_action }}</td>
                </tr>
                <tr>
                    <th>Dokumentasi Foto</th>
                    <td>
                        <img width="250px" src="{{ asset($nearmiss->link()) }}" alt="{{ $nearmiss->photo }}" >
                    </td>
                </tr>
            </table>
        </div>
    </div>

@endsection
    