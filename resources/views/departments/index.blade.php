@extends('admin_template')

@section('content')

    <div class="box">
        <div class="box-header">
          <h3 class="box-title">
              Tabel Departemen
              <a href="{{ route('departments.create') }}" 
                 class="btn btn-primary btn-sm">
                  <i class="fa fa-plus"></i>
              </a>
          </h3>
        </div>
        <!-- /.box-header -->
            
        <div class="box-body" style="overflow-x: scroll">
            <table id="departmentTable" class="table table-bordered table-striped">
                <thead>
                    <tr>
                        <th>ID</th>
                        <th>Name</th>
                        <th>Action</th>
                    </tr>
                </thead>
                <tbody>
                    @foreach( $departments as $department )
                    <tr>
                        <td>{{ $department->id }}</td>
                        <td>{{ $department->name }}</td>
                        <td>
                            <a href="{{ route('departments.edit', ['id'=>$department->id]) }}" title="Edit"
                               class="btn btn-sm btn-primary">
                                <i class="fa fa-edit"></i>
                            </a>
                            <form onsubmit="return confirm('Hapus data?');" 
                                  id="delete-{{ $department->id }}"
                                  class="hide"
                                  method="POST" 
                                  action="{{ route('departments.destroy', ['id'=>$department->id]) }}">
                                {{ method_field('DELETE') }}
                                {{ csrf_field() }}
                            </form>
                            <a href="javascript:void(0)" title="Hapus"
                               onclick="$('#delete-{{ $department->id }}').submit()"
                               class="btn btn-sm btn-danger">
                                <i class="fa fa-remove"></i>
                            </a>
                        </td>
                    </tr>
                    @endforeach
                </tbody>
            </table>
        </div>
        <!-- /.box-body -->
    </div>

@endsection

@section('inline-script')
    <script type="text/javascript">
        $(document).ready(function(){
            $('#departmentTable').DataTable({
                responsive: true,
                'aoColumnDefs': [{
                    'bSortable': false,
                    'aTargets': [ -1 ]
                }]
            });
        });
    </script>
@endsection