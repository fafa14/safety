@extends('admin_template')

@section('content')

    <div class="box">
        <div class="box-header">
          <h3 class="box-title">
              Form Edit Departemen
          </h3>
        </div>
        <!-- /.box-header -->
            
        <div class="box-body">
            {!! form($form) !!}
        </div>
    </div>

    <div class="box">
        <div class="box-header">
          <h3 class="box-title">
            Daftar Pegawai di Departemen
          </h3>
        </div>
        <!-- /.box-header -->
            
        <div class="box-body">
            @if($head)
                <dl class="dl-horizontal Bold">
                    <dt>Kepala / Responsible Person</dt><dd>{{ $head->name }}</dd>
                </dl>
            @endif
            <table id="userTable" class="table table-bordered table-striped">
                <thead>
                    <tr>
                        <td>BN</td>
                        <td>Nama</td>
                        <td>Action</td>
                    </tr>
                </thead>
                <tbody>
                    @foreach( $users as $user )
                        <tr>
                            <td>{{ $user->bn }}</td>
                            <td>
                                {{ $user->name }}
                                @if($head && $user->id == $head->id)
                                    <i class="fa fa-user-circle"></i>
                                @endif
                            </td>
                            <td>
                                <form onsubmit="return confirm('Pilih sebagai kepala departemen?');" 
                                        id="sethead-{{ $user->id }}"
                                        class="hide"
                                        method="POST" 
                                        action="{{ route('sethead', ['id'=>$department->id, 'user_id'=>$user->id]) }}">
                                      {{ method_field('PUT') }}
                                      {{ csrf_field() }}
                                </form>
                                <a href="javascript:void(0)" 
                                   onclick="$('#sethead-{{ $user->id }}').submit()"
                                   class="btn btn-sm btn-default" title="Set Kepala">
                                    <i class="fa fa-star"></i>
                                </a>
                            </td>
                        </tr>
                    @endforeach
                </tbody>
            </table>
        </div>
    </div>

@endsection

@section('inline-script')
    <script type="text/javascript">
        $(document).ready(function(){
            $('#userTable').DataTable({
                'aoColumnDefs': [{
                    'bSortable': false,
                    'aTargets': [ -1 ]
                }]
            });
        });
    </script>
@endsection