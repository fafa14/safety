@extends('mail.base_mail')

@section('content')
    <h1>
        Notifikasi Nearmiss
    </h1>
    <p>
        Pengguna {{ $user->name }} telah mengisi nearmiss pada pukul {{ $nearmiss->created_at }} dengan data sebagai berikut:<br/>
        <dl class="dl-horizontal">
            <dt>Lokasi</dt><dd>{{ $nearmiss->location->name }}</dd>
            <dt>Detail lokasi</dt><dd>{{ $nearmiss->location_detail }}</dd>
            <dt>Waktu kejadian</dt><dd>{{ $nearmiss->ndate }} {{ $nearmiss->ntime }}</dd>
            <dt>Deskripsi kejadian</dt><dd>{{ $nearmiss->description }}</dd>
        </dl>
        <a href="{{ route('nearmisses.show', ['id'=>$nearmiss->id]) }}">Klik disini</a>
        untuk melihat data secara detail.
    </p>
@endsection