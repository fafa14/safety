<!DOCTYPE html>

<html>
    <head>
        <meta charset="UTF-8">
        <title>Email Pemberitahuan</title>
        <meta content='width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no' name='viewport'>
    </head>
    <body>
        @yield('content')
    </body>
</html>