@extends('admin_template')

@section('content')
<div class="container">
    <div class="row justify-content-center">
            <div class="card">
                <div class="card-header">
                    Selamat datang {{ auth()->user()->name }} !
                    <br/><br/>
                </div>

                <div class="card-body">
                    <div class="row">
                        <div class="col-md-3 col-sm-4 col-xs-12">
                            <div class="info-box">
                                <span class="info-box-icon bg-aqua">
                                    <i class="fa fa-ambulance"></i>
                                </span>
                                @php( $count_nearmiss = count(auth()->user()->nearmissCreated) )
                                @role(['admin', 'superadmin', 'safety'])
                                    @php( $count_nearmiss = $count_all_nearmiss )
                                @endrole

                                <div class="info-box-content">
                                    <span class="info-box-text">Nearmiss</span>
                                    <span class="info-box-number">
                                        @role(['admin', 'superadmin', 'safety'])
                                            {{ count(auth()->user()->nearmissCreated) }} /
                                        @endrole
                                        {{ $count_nearmiss }}
                                    </span>
                                    <div class="pull-right">
                                        <a href="{{ route('nearmisses.index') }}"
                                           class="btn btn-default btn-sm">
                                            <i class="fa fa-list"></i>
                                        </a>
                                        <a href="{{ route('nearmisses.create') }}" title="Buat Baru"
                                           class="btn btn-primary btn-sm">
                                            <i class="fa fa-plus"></i>
                                        </a>
                                    </div>
                                </div>
                                <!-- /.info-box-content -->
                            </div>
                            <!-- /.info-box -->
                        </div>

                        @role(['admin', 'superadmin', 'safety'])
                        <div class="col-md-3 col-sm-4 col-xs-12">
                            <div class="info-box">
                                <span class="info-box-icon bg-olive">
                                    <i class="fa fa-check"></i>
                                </span>

                                <div class="info-box-content">
                                    <span class="info-box-text">Walkdown</span>
                                    <span class="info-box-number">{{ $count_all_walkdown }}</span>
                                    <div class="pull-right">
                                        <a href="{{ route('checklists.index') }}"
                                           class="btn btn-default btn-sm">
                                            <i class="fa fa-list"></i>
                                        </a>
                                        @role('safety')
                                        <a href="{{ route('checklists.create') }}" title="Buat Baru"
                                           class="btn btn-primary btn-sm">
                                            <i class="fa fa-plus"></i>
                                        </a>
                                        @endrole
                                    </div>
                                </div>
                                <!-- /.info-box-content -->
                            </div>
                            <!-- /.info-box -->
                        </div>
                        @endrole
                    </div>
                </div>
            </div>
    </div>
</div>
@endsection
