@extends('admin_template')

@section('content')

    <div class="box">
        <div class="box-header">
            <h3 class="box-title">
                Form
            </h3>
        </div>
        <div class="box-body">
            <table id="table" class="table table-bordered table-striped">
                <tr>
                    <th>Kategori</th>
                    <td>{{ $item->category }}</td>
                </tr>
                <tr>
                    <th>Pertanyaan</th>
                    <td>{{ $item->question }}</td>
                </tr>
            </table>
        </div>
        <!-- /.box-header -->

        <div class="box-body">
            {!! form_start($form) !!}
            {!! form_row($form->answer) !!}
            {!! form_row($form->photo) !!}
            {!! form_row($form->detail) !!}
            @role('safety')
            <div class="row">
                <div class="col-sm-12 col-md-12 col-lg-12" style="margin-bottom: 10px">
                    {!! form_row($form->submit) !!}
                </div>
            </div>
            @endrole
            {!! form_end($form, $renderRest = false) !!}
        </div>
    </div>

@endsection