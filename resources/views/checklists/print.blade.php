@extends('print_template')

@section('content')
    <div class="row">
        <div class="col-sm-12 invoice-col">
            <b>CETAK CHECKLIST</b><br/><br/>
        </div>
    </div>
    <table class="table table-bordered table-striped">
        <tr>
            <th>ID Checklist</th>
            <td>{{ $checklist->id }}</td>
        </tr>
        <tr>
            <th>Inspector</th>
            <td>
                @include( 'walkdowninspectors.inspector_list')
            </td>
        </tr>
        <tr>
            <th>Lokasi</th>
            <td>{{ $checklist->location->name }}</td>
        </tr>
        <tr>
            <th>Tanggal</th>
            <td>{{ $checklist->inspection_date }}</td>
        </tr>
    </table>
    <table id="checklistTable" class="table table-bordered table-striped">
        <thead>
        <tr>
            <th>No</th>
            <th>Pertanyaan</th>
            <th>Jawaban</th>
            <th>Detail</th>
            <th>Photo</th>
        </tr>
        </thead>
        <tbody>
        @php( $inc1 = 1 )
        @foreach( $checklist->getItemsIndexByCategoryForPrint() as $key => $items )
            <tr>
                <td><strong>{{ $inc1 }}</strong></td>
                <td colspan="3"><strong>{{ $key }}</strong></td>
            </tr>
            @php( $inc2 = 1 )
            @foreach($items as $item)
                <tr>
                    <td>{{ $inc1 }}.{{ $inc2 }}</td>
                    <td>{{ $item->question }}</td>
                    <td>
                        {{ $item->answerString() }}<br/>
                    </td>
                    <td>{{ $item->detail }}</td>
                    <td>
                        @if( $item->photo )
                            <img style="width: 200px;" src="{{ asset($item->tumbnail()) }}" alt="img"/>
                        @endif
                    </td>
                </tr>
                @php( $inc2++ )
            @endforeach
            @php( $inc1++ )
        @endforeach
        </tbody>
    </table>
@endsection