@extends('admin_template')

@section('content')

    <div class="box">
        <div class="box-header">
            <h3 class="box-title">
                Tabel Checklist
                @if( auth()->user()->hasRole('safety') )
                    <a href="{{ route('checklists.create') }}"
                       class="btn btn-primary btn-sm">
                        <i class="fa fa-plus"></i>
                    </a>
                @endif
            </h3>
        </div>
        <!-- /.box-header -->

        <div class="box-body" style="overflow-x: scroll">
            <table id="checklistTable" class="table table-bordered table-striped">
                <thead>
                <tr>
                    <th>ID</th>
                    <th>Inspector</th>
                    <th>Lokasi</th>
                    <th>Tanggal</th>
                    <th>Action</th>
                </tr>
                </thead>
                <tbody>
                @foreach( $checklists as $checklist )
                    <tr>
                        <td>{{ $checklist->id }}</td>
                        <td>{{ $checklist->creator->name }}</td>
                        <td>
                            {{ $checklist->location->name }}<br/>
                            <i class="small">{{ $checklist->location_detail }}</i>
                        </td>
                        <td>
                            {{ $checklist->inspection_date }}
                        </td>
                        <td>
                            <a href="{{ route('checklists.show', ['id'=>$checklist->id]) }}" title="Tampilkan Checklist"
                               class="btn btn-sm btn-primary">
                                <i class="fa fa-check"></i> Checklist
                            </a>
                            <a href="{{ route('temuans.show', ['id'=>$checklist->temuan->id]) }}" title="Tampilkan Temuan"
                               class="btn btn-sm btn-primary">
                                <i class="fa fa-search"></i> Temuan
                            </a>
                            <a href="{{ route('checklists.edit', ['id'=>$checklist->id]) }}" title="Edit Data Walkdown"
                               class="btn btn-sm btn-primary">
                                <i class="fa fa-edit"></i>
                            </a>
                            <a href="{{ route('walkdowninspectors.list', ['id'=>$checklist->id]) }}" title="Tambah data Inspector"
                               title="Daftar Inspector"
                               class="btn btn-sm btn-default">
                                <i class="fa fa-users"></i>
                            </a>
                            <span class="dropdown">
                                <button id="dropdownPrint" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false" class="btn btn-default dropdown-toggle" type="button">
                                    <i class="fa fa-print"></i>
                                </button>
                                <ul class="dropdown-menu">
                                    <li><a href="{{ route('checklists.print', ['id'=>$checklist->id]) }}" target="_blank">Checklist</a></li>
                                    <li><a href="{{ route('temuans.print', ['id'=>$checklist->temuan->id]) }}" target="_blank">Temuan</a></li>
                                </ul>
                            </span>
                            <form onsubmit="return confirm('Hapus Checklist?');"
                                  id="delete-{{ $checklist->id }}"
                                  class="hide"
                                  method="POST"
                                  action="{{ route('checklists.destroy', ['id'=>$checklist->id]) }}">
                                {{ method_field('DELETE') }}
                                {{ csrf_field() }}
                            </form>
                            <a href="javascript:void(0)" title="Hapus"
                               onclick="$('#delete-{{ $checklist->id }}').submit()"
                               class="btn btn-sm btn-danger">
                                <i class="fa fa-remove"></i>
                            </a>
                        </td>
                    </tr>
                @endforeach
                </tbody>
            </table>
        </div>
        <!-- /.box-body -->
    </div>

@endsection

@section('inline-script')
    <script type="text/javascript">
        $(document).ready(function(){
            $('#checklistTable').DataTable({
                responsive: true,
                'aoColumnDefs': [{
                    'bSortable': false,
                    'aTargets': [ -1 ]
                }]
            });
        });
    </script>
@endsection