@extends('admin_template')

@section('content')

    <div class="box">
        <div class="box-header">
          <h3 class="box-title">
              Form Checklist
          </h3>
        </div>
        <!-- /.box-header -->
            
        <div class="box-body">
            {!! form($form) !!}
        </div>
    </div>

@endsection

@section('inline-script')
    <script type="text/javascript">
        $(document).ready(function(){
            //Date picker
            $('.datepicker').datepicker({
                dateFormat: 'yy-mm-dd'
            });
        });
    </script>
@endsection