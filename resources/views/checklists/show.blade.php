@extends('admin_template')

@section('content')

    <div class="box">
        <div class="box-header">
            <h3 class="box-title">
                Detail Checklist
                <a href="{{ route('checklists.print', ['id'=>$checklist->id]) }}" target="_blank"
                   class="btn btn-sm btn-default">
                    <i class="fa fa-print"></i> Cetak
                </a>
            </h3>
        </div>
        <div class="box-body">
            <table id="checklistTable" class="table table-bordered table-primary">
                <tr>
                    <th>ID</th>
                    <td>{{ $checklist->id }}</td>
                </tr>
                <tr>
                    <th>Inspector</th>
                    <td>
                        @include( 'walkdowninspectors.inspector_list')
                    </td>
                </tr>
                <tr>
                    <th>Tanggal</th>
                    <td>{{ $checklist->inspection_date }}</td>
                </tr>
                <tr>
                    <th>Lokasi</th>
                    <td>{{ $checklist->location->name }}</td>
                </tr>
                <tr>
                    <th>Area</th>
                    <td>{{ $checklist->location_detail }}</td>
                </tr>
            </table>
        </div>
    </div>

    <div class="box">
        <div class="box-header">
            <h3 class="box-title">
                Daftar Item Cheklist
                @php( $itemUnanswered = $checklist->getItemUnanswered())
                @role('safety')
                    @if( $itemUnanswered != null )
                        <a href="{{ route('checklistitems.edit', ['id'=>$itemUnanswered->id]) }}" title="Mulai Isi Checklist"
                           class="btn btn-primary btn-sm">
                            <i class="fa fa-play"></i>
                        </a>
                    @endif
                @endrole
            </h3>
        </div>
        <div class="box-body" style="overflow-x: scroll">
            <table id="checklistTable" class="table table-bordered table-responsive">
                <thead>
                <tr>
                    <th>No</th>
                    <th>Pertanyaan</th>
                    <th>Jawaban (Ya/Tidak)</th>
                    <th>Photo</th>
                    @role('safety')
                        <th>Action</th>
                    @endrole
                </tr>
                </thead>
                <tbody>
                @php( $inc1 = 1 )
                @foreach( $checklist->getChecklistItemCategory() as $key => $category )
                    <tr>
                        <td><strong>{{ $inc1 }}</strong></td>
                        <td colspan="4">
                            <strong>{{ $category }}</strong>
                            <form onsubmit="return confirm('Semua data yg telah diisi pada kategori ini akan terhapus, tandai tidak diisi?');"
                                  id="skip-{{ $key }}"
                                  class="hide"
                                  method="POST"
                                  action="{{ route('checklistitems.skip', ['id'=>$key, 'action'=>'skip']) }}">
                                {{ method_field('POST') }}
                                {{ csrf_field() }}
                            </form>
                            <a href="javascript:void(0)"
                               onclick="$('#skip-{{ $key }}').submit()"
                               class="btn btn-xs btn-warning">
                                <i class="fa fa-check-circle-o"></i> tandai tidak diisi
                            </a>
                        </td>
                    </tr>
                    @php( $inc2 = 1 )

                    @foreach($checklist->getChecklistItemsByCategoryId($key) as $item)
                        <tr>
                            <td>{{ $inc1 }}.{{ $inc2 }}</td>
                            <td>
                                {{ $item->question }}<br/>
                            </td>
                            <td>
                                {{ $item->answerString() }}<br/>
                                <i style="color: dodgerblue" class="small">{{ $item->detail }}</i>
                            </td>
                            <td>
                                @if( $item->photo )
                                    <a href="{{ asset($item->link()) }}" data-featherlight="image">
                                        <img class="img-circle" style="width: 50px; height: 50px;" src="{{ asset($item->tumbnail()) }}" alt="img"/>
                                    </a>
                                @endif
                            </td>
                            @role('safety')
                            <td>
                                <a href="{{ route('checklistitems.edit', ['id'=>$item->id]) }}"
                                   class="btn btn-sm btn-primary">
                                    <i class="fa fa-edit"></i>
                                </a>
                            </td>
                            @endrole
                        </tr>
                        @php( $inc2++ )
                    @endforeach
                    @php( $inc1++ )
                @endforeach
                </tbody>
            </table>
            <br/>
        </div>
    </div>

    <div class="box">
        <div class="box-header">
            <h3 class="box-title">Kategori Ditandai tidak diisi</h3>
        </div>
        <div class="box-body">
            <table class="table table-bordered table-striped">
                <thead>
                <tr>
                    <th>Nama Kategori</th>
                </tr>
                </thead>
                <tbody>
                @foreach($checklist->getSkipedCategories() as $key => $category)
                    <tr>
                        <td>
                            {{ $category }}
                            <form onsubmit="return confirm('Tandai harus diisi?');"
                                  id="unskip-{{ $key }}"
                                  class="hide"
                                  method="POST"
                                  action="{{ route('checklistitems.skip', ['id'=>$key, 'action'=>'unskip']) }}">
                                {{ method_field('POST') }}
                                {{ csrf_field() }}
                            </form>
                            <a href="javascript:void(0)"
                               onclick="$('#unskip-{{ $key }}').submit()"
                               class="btn btn-xs btn-success">
                                <i class="fa fa-check-circle-o"></i> tandai harus diisi
                            </a>
                        </td>
                    </tr>
                @endforeach
                </tbody>
            </table>
        </div>
    </div>
@endsection