@extends('admin_template')

@section('content')

    <div class="box">
        <div class="box-header">
          <h3 class="box-title">
              Ubah Role Pengguna
          </h3>
        </div>
        
        <div class="box-body">
            <form method="POST" action="{{ route('save-roles', ['id' => $user->id]) }}">
                {{ csrf_field() }}
                <div class="form-group">
                    <label for="name" class="control-label required">Role</label>
                    <select id="role" name="role[]" class="form-control select2">
                        @foreach( $roles as $role )
                            <option value="{{ $role->id }}">
                                {{ $role->name }} 
                            </option>
                        @endforeach
                    </select>
                </div>
                <div class="form-group">
                    <input  type="submit" class="form-control" value="Simpan"/>
                </div>
            </form>
        </div>
    </div>
    
@endsection

@section('inline-script')
    <script type="text/javascript">
        $(document).ready(function(){
            // $('#role').select2({
            //     multiple: true,
            //     placeholder: 'Pilih Role..',
            //     tokenSeparators: [','],
            // });
            
            {{--var values = new Array();--}}
            {{--@foreach( $user_roles as $role )--}}
                {{--values.push({{ $role }});--}}
            {{--@endforeach--}}
            {{--$("#role").val(values).trigger('change');--}}
            
        });
    </script>
@endsection