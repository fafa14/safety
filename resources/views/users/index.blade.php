@extends('admin_template')

@section('content')

    <div class="box">
        <div class="box-header">
          <h3 class="box-title">
              Tabel Pengguna
              <a href="{{ route('users.create') }}" class="btn btn-primary btn-sm"><i class="fa fa-plus"></i> Pengguna Baru</a>
          </h3>
        </div>
        <!-- /.box-header -->
            
        <div class="box-body" style="overflow-x: scroll">
            <table id="userTable" class="table table-bordered table-striped">
                <thead>
                    <tr>
                        <th>ID</th>
                        <th>Departemen</th>
                        <th>Name</th>
                        <th>Email</th>
                        <th>Action</th>
                    </tr>
                </thead>
                <tbody>
                    @foreach( $users as $user )
                    <tr>
                        <td>{{ $user->id }}</td>
                        <td>{{ $user->department->name }}</td>
                        <td>
                            {{ $user->bn }}<br/>
                            {{ $user->name }}
                            <br/>
                            @foreach( $user->roles as $role )
                                @if(Auth::user()->hasRole('superadmin'))
                                    <div class="btn-group">
                                        <a href="javascript:void(0)" type="button" class="btn btn-default btn-xs">{{ $role->name }}</a>
                                    </div>
                                @elseif(Auth::user()->hasRole('admin'))
                                    @if($role->name != 'admin' || $role->name != 'superadmin' )
                                        <div class="btn-group">
                                            <a href="javascript:void(0)" type="button" class="btn btn-default btn-xs">{{ $role->name }}</a>
                                        </div>
                                    @endif
                                @endif
                            @endforeach
                        </td>
                        <td>{{ $user->email }}</td>
                        <td>
                            <a href="{{ route('users.edit', ['id'=>$user->id]) }}"
                               title="Edit Data"
                               class="btn btn-sm btn-primary">
                                <i class="fa fa-edit"></i>
                            </a>
                            <form class="hide" id="delete-{{ $user->id }}"
                                  onsubmit="return confirm('Hapus User?');"
                                  method="POST" action="{{ route('users.destroy', ['id'=>$user->id]) }}">
                                {{ method_field('DELETE') }}
                                {{ csrf_field() }}
                            </form>
                            @if( $user->id != Auth::user()->id )
                                <a href="{{ route('change-roles', ['id'=>$user->id]) }}"
                                   title="Ubah Role User"
                                    class="btn btn-sm btn-primary">
                                     <i class="fa fa-user-o"></i>
                                </a>
                                @if(!$user->hasRole('superadmin'))
                                <a href="javascript:void(0)" onclick="$('#delete-{{ $user->id }}').submit();" title="Hapus"
                                   class="btn btn-sm btn-danger">
                                    <i class="fa fa-remove"></i>
                                </a>
                                @endif
                                @if( Auth::user()->hasRole('superadmin') or Auth::user()->hasRole('admin') )
                                    <a onclick="return confirm('Login sebagai {{ $user->name }}?')"
                                       title="Login sebagai {{ $user->name }}"
                                       href="{{ route('loginbyid', ['id'=>$user->id]) }}"
                                       class="btn btn-sm btn-warning">
                                        <i class="fa fa-user-circle-o"></i>
                                    </a>
                                    @if(!$user->hasRole('superadmin'))
                                        <a onclick="return confirm('Reset password {{ $user->name }}?')"
                                           title="Reset password {{ $user->name }}"
                                           href="{{ route('reset-password', ['id'=>$user->id]) }}"
                                           class="btn btn-sm btn-warning">
                                            <i class="fa fa-refresh"></i>
                                        </a>
                                    @endif
                                @endif
                            @endif
                        </td>
                    </tr>
                    @endforeach
                </tbody>
            </table>
        </div>
        <!-- /.box-body -->
    </div>

@endsection

@section('inline-script')
    <script type="text/javascript">
        $(document).ready(function(){
            $('#userTable').DataTable({
                responsive: true,
                'aoColumnDefs': [{
                    'bSortable': false,
                    'aTargets': [ -1]
                }]
            });
        });
    </script>
@endsection