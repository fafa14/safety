@extends('admin_template')

@section('content')

    <div class="box">
        <div class="box-header">
          <h3 class="box-title">
              Tabel Lokasi
              <a href="{{ route('locations.create') }}" 
                 class="btn btn-primary btn-sm">
                  <i class="fa fa-plus"></i>
              </a>
          </h3>
        </div>
        <!-- /.box-header -->
            
        <div class="box-body" style="overflow-x: scroll">
            <table id="locationTable" class="table table-bordered table-striped">
                <thead>
                    <tr>
                        <th>ID</th>
                        <th>Name</th>
                        <th>Peerson Responsible</th>
                        <th>Action</th>
                    </tr>
                </thead>
                <tbody>
                    @foreach( $locations as $location )
                    <tr>
                        <td>{{ $location->id }}</td>
                        <td>{{ $location->name }}</td>
                        <td>
                            @if($location->user_id != null)
                                {{ $location->responsiblePerson->name }}
                            @endif
                        </td>
                        <td>
                            <a href="{{ route('locations.edit', ['id'=>$location->id]) }}" title="Edit"
                               class="btn btn-sm btn-primary">
                                <i class="fa fa-edit"></i>
                            </a>
                            <form onsubmit="return confirm('Hapus data?');" 
                                  id="delete-{{ $location->id }}"
                                  class="hide"
                                  method="POST" 
                                  action="{{ route('locations.destroy', ['id'=>$location->id]) }}">
                                {{ method_field('DELETE') }}
                                {{ csrf_field() }}
                            </form>
                            <a href="javascript:void(0)" title="Hapus"
                               onclick="$('#delete-{{ $location->id }}').submit()"
                               class="btn btn-sm btn-danger">
                                <i class="fa fa-remove"></i>
                            </a>
                        </td>
                    </tr>
                    @endforeach
                </tbody>
            </table>
        </div>
        <!-- /.box-body -->
    </div>

@endsection

@section('inline-script')
    <script type="text/javascript">
        $(document).ready(function(){
            $('#locationTable').DataTable({
                responsive: true,
                'aoColumnDefs': [{
                    'bSortable': false,
                    'aTargets': [ -1 ]
                }]
            });
        });
    </script>
@endsection