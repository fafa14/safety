@extends('admin_template')

@section('content')

    <div class="box">
        <div class="box-header">
            <h3 class="box-title">
                Detail Kategori
            </h3>
        </div>
        <div class="box-body">
            <table id="categoryTable" class="table table-bordered table-striped">
                <tr>
                    <th>Nama</th>
                    <td>{{ $category->name }}</td>
                </tr>
            </table>
        </div>
    </div>
    <div class="box">
        <div class="box-header">
            <h3 class="box-title">
                Tabel Master Ceklist
                <a href="{{ route('mastercheklists-create-by-category', ['id'=>$category->id]) }}"
                   title="Tambah Baru"
                   class="btn btn-primary btn-sm">
                    <i class="fa fa-plus"></i>
                </a>
            </h3>
        </div>
        <div class="box-body" style="overflow-x: scroll">
            <table id="checklistTable" class="table table-bordered table-striped">
                <thead>
                    <tr>
                        <th>ID</th>
                        <th>Pertanyaan</th>
                        <th>Action</th>
                    </tr>
                </thead>
                <tbody>
                    @foreach( $masterchecklists as $masterchecklist )
                        <tr>
                            <td>{{ $masterchecklist->id }}</td>
                            <td>{{ $masterchecklist->question }}</td>
                            <td>
                                <a href="{{ route('masterchecklists.edit', ['id'=>$masterchecklist->id]) }}" title="Edit"
                                    class="btn btn-sm btn-primary">
                                     <i class="fa fa-edit"></i>
                                 </a>
                                 <form onsubmit="return confirm('Hapus data?');" 
                                       id="delete-{{ $masterchecklist->id }}"
                                       class="hide"
                                       method="POST" 
                                       action="{{ route('masterchecklists.destroy', ['id'=>$masterchecklist->id]) }}">
                                     {{ method_field('DELETE') }}
                                     {{ csrf_field() }}
                                 </form>
                                 <a href="javascript:void(0)" title="Hapus"
                                    onclick="$('#delete-{{ $masterchecklist->id }}').submit()"
                                    class="btn btn-sm btn-danger">
                                     <i class="fa fa-remove"></i>
                                 </a>
                                
                            </td>
                        </tr>
                    @endforeach
                </tbody>
            </table>
        </div>
    </div>
@endsection

@section('inline-script')
    <script type="text/javascript">
        $(document).ready(function(){
            $('#checklistTable').DataTable({
                responsive: true,
                'aoColumnDefs': [{
                    'bSortable': false,
                    'aTargets': [ -1 ]
                }]
            });
        });
    </script>
@endsection