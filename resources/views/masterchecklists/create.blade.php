@extends('admin_template')

@section('content')

    <div class="box">
        <div class="box-header">
          <h3 class="box-title">
              Form Master Ceklist Baru
          </h3>
        </div>
        <!-- /.box-header -->
            
        <div class="box-body">
            {!! form($form) !!}
        </div>
    </div>

@endsection